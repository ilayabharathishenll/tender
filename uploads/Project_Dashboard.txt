DataVault Project update:


Here are the few points to highlight:

1. The tech stack selection has been done. (Running on AWS with Node js / React js / Dynamo DB)

2. Our complete source repo is available on BitBucket.

3. We use Circle CI as a build tool.

4. Using Circle CI, we made a hook to trigger Unit test script to run automatically on every commits to Git. IFF the test cases are successful the code will get pushed to Git repo.

5. Our communication channel is Slack for any business/technical/project discussions.

6. We have added a set of Slack bots to the channel such as Git commits, Drive file upload.

7. We are working on the two weeks sprint model (and weekly status sync).

8. We started our code reviews (Dev team and then our core team does the review)

9. The project has been hosted into AWS with staging and prod mode.
     
Prod: http://datavaultcenter.com/
     
Staging:  http://staging.datavaultcenter.com/login

10. For the testing aspects, we have discussed and prepared the base framework and test case templates in Cucumber framework.

===========

*DataVault Project calendar*

Project calendar:
 
June 9   > review of iteration #1.
 
June 30 > ETA for Milestone #1 completion.
 
July 20  > Mobile app discussion will start.
 
July 20  > Actual testing will start.
 
July  31 > All APIs to be ready.
 
Aug  6   > Mobile dev to start.
 
Aug  24 > Feature complete. Ready to release.
 
Sep  10 > UAT iteration #1. (This will be first look to get the feedback and check if any process flow changes required)
 Sep  30 > Our QA complete.
 
Oct  1    > Actual UAT
 
Oct  17  > Beta launch
Shall we plan to have our security audit by Sep 3rd week.