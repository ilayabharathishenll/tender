<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url();?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Tender Details (Individual Tender)</div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" action="<?php echo base_url()?>supplier/equote" class="horizontal-form" method="POST">
                <div class="form-body">
                    <h3 class="form-section formheading">Tender Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                            	<label class="control-label col-md-4">Part No</label>
                                <div class="col-md-8">
                                	<input type="text" class="form-control" name="part_no" id="part_no" placeholder="" value="Tender-001">
                                </div>
                                
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="sel_partname" id="sel_partname">
                                        <option value="">----Select Part Name----</option>
                                        <option value="1" selected>Website Tender</option>
                                        <option value="2">Part two</option>
                                        <option value="3">Part three</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                            	<label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="" value="ADMINTENDER-001">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Base Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="base_price" id="base_price" placeholder="" value="100000">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                            	<label class="control-label col-md-4">Select Qunatity</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="sel_qunatity" id="sel_qunatity">
                                        <option value="">----Select Qunatity----</option>
                                        <option value="1">1</option>
                                        <option value="2" selected>2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_duration" id="delivery_duration" placeholder="Delivery Duration in Days" value="10">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                     
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" value="26/05/2018">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" value="28/05/2018">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <h3 class="form-section formheading">Tender Detailed Description or Information</h3>
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="tender_detail" id="tender_detail" rows="5" cols="50" placeholder="Enter the Tender Details....">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <!-- <span class="help-block pull-right">(Upload Multiple Document's)</span> -->
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Terms and Condition</label>
                                <div class="col-md-5">
                                    <a href="<?php echo base_url();?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div> -->
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <!-- <span class="help-block pull-right">(Upload Multiple Document's)</span> -->
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <div class="col-md-5">
                                   
                                   <a href="<?php echo base_url();?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="helpmore" id="helpmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div> -->
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Detail</label>
                                <div class="col-md-5">
                                    <a href="<?php echo base_url();?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="detailmore" id="detailmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div> -->
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Documents Required</label>
                                <div class="col-md-5">

                                    <a href="<?php echo base_url();?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdocmore" id="tenderdocmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div> -->
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <a href="<?php echo base_url();?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoconemore" id="tenderdoconemore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div> -->
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <a href="<?php echo base_url();?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoctwomore" id="tenderdoctwomore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div> -->
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Price</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tender_price" id="tender_price" autocomplete="off" value="10000">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">No of Qunatity</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="qunatity_no" id="qunatity_no" autocomplete="off" value="10000">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter Your Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="your_price" id="your_price" autocomplete="off" value="10000">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                           
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Apply Tender
                    </button>
                   <!--  <a href="preview_tender.php" type="button" class="btn red customrestbtn" > <i class="fa fa-eye"></i> Preview Tender</a> -->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->