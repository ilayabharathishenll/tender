<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tender Management</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Tender Management Admin Theme for " name="description" />
        <meta content="" name="author" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/pages/css/login-3.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/layouts/layout/css/login_style.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" /> 

    </head>
    <body class=" login">
        <!-- BEGIN LOGO -->
       <div class="logo">
            <!-- <a href="javascript:void(0);">
                <img src="<?php echo base_url();?>assets/pages/img/logo-big.png" alt="" /> 
            </a> -->
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <div class="logo" style="margin: 10px auto 19px;">
                <a href="javascript:void(0);">
                    <img src="<?php echo base_url();?>assets/pages/img/logo-big.png" alt="" /> 
                </a>
            </div>
            <form class="login-form" action="" method="post">
                <h3 class="form-title">Supplier Login</h3>
                <?php
                $msg=$this->session->flashdata('message');
                $supliermsg=$this->session->flashdata('message_login');
                if(!empty($msg)) {
                ?>  
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $msg ?>
                    </div>
                <?php
                }
                ?>
                <div class="alert alert-danger display-hide" style="<?php echo $supliermsg ?>">
                    <button class="close" data-close="alert"></button>
                    <span> Invalid email and password. </span>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Enter Your Registered Email ID" name="email" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" 
                        placeholder="Enter Your Password" name="password" /> </div>
                </div>
                <div class="form-actions" style="border-bottom: 0px solid #eee;padding: 0 30px 13px;">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Remember me
                        <span style="border: 1px solid #000;"></span>
                    </label>
                </div>
                <div class="form-actions" style="border-bottom: 0px solid #eee;padding: 0 30px 13px;">
                    <button type="submit" class="btn green customlogin" name="submit" value="login"> <i class="fa fa-sign-in"></i> Login</button>
                    <button type="button" class="btn green pull-right customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
                </div>
                <div class="forget-password" style="margin-top: 14px;">
                    <a href="<?php echo base_url()?>supplier/login/forgot_password" class="forgetpassword"><u>Forgot Password?</u></a>
                    <a href="<?php echo base_url()?>supplier/login/register" class="pull-right forgetpassword"><u>Register for Supplier</u></a>
                    <p></p>
                </div>
            </form>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/pages/scripts/login.min.js" type="text/javascript"></script>
    </body>
</html>