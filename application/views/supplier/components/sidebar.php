<?php
error_reporting(0);
$navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
if (is_numeric($navigationURL)) {
    $navigationURL=$this->uri->segment(3);
}
?>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='dashboard')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/dashboard" class="nav-link nav-toggle">
                    <i class="dashboard-image"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='equote' || $navigationURL=='single_quote' || $navigationURL=='preview_tender')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/equote" class="nav-link nav-toggle">
                    <i class="icon-tender"></i>
                    <span class="title">E Quote</span>
                    <span class="selected"></span>
                </a>
            </li>
            
            <li class="nav-item <?php echo ($navigationURL=='tenders' || $navigationURL=='changetender' || $navigationURL=='change_tender')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/tenders" class="nav-link nav-toggle">
                    <i class="icon-applied"></i>
                    <span class="title">Applied Tenders</span>
                    <span class="selected"></span>
                </a>
            </li>
           
            <li class="nav-item <?php echo ($navigationURL=='award')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/award" class="nav-link nav-toggle">
                    <i class="icon-award"></i>
                    <span class="title">Award Tenders</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='bidding'|| $navigationURL=='changebidding' || $navigationURL=='previewbidding')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/bidding" class="nav-link nav-toggle">
                    <i class="icon-bidding"></i>
                    <span class="title">Bidding</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='help_desk.php')?'active open':''; ?>">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="icon-help"></i>
                    <span class="title">Help Desk</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='profile')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/profile" class="nav-link nav-toggle">
                    <i class="icon-setting"></i>
                    <span class="title">Profile Settings</span>
                    <!--  <span class="arrow"></span> -->
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
    </div>
</div>