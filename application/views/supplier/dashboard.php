<?php
//print_r($this->session->userdata());
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content"  id="dashboard">
	<div class="row widget-row">
        <?php
        $msg=$this->session->flashdata('success');
        if(!empty($msg)){
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
            </div>
        </div>
        <?php
        }
        ?>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat red text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Latest E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-social-dropbox redicon dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="25">25</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat bg-green-jungle text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Applied E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-layers font-green-jungle dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="15">15</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat blue text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No of Approval E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white font-blue icon-drawer dashboardborder"></i>
                    <div class="widget-thumb-body">
                       <!--  <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="07">07</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat  yellow-casablanca text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Awards E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-badge dashboardborder font-yellow-casablanca"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="20">20</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat purple-intense  text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Bidding</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white fa fa-gavel dashboardborder font-purple-intense "></i>
                    <div class="widget-thumb-body">
                       <!--  <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="10">10</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat grey-cascade text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Help Desk</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-screen-desktop dashboardborder font-grey-cascade"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="08">08</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat red-soft text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Supplier Document</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon red-soft"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat"><a href="sample.doc"><i class="fa fa-download" style="color:white;"></i></a></span>
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <!-- <span class="widget-thumb-body-stat" data-counter="counterup" data-value="08">08</span> -->
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat blue-hoki text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Supplier Upload Document</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon blue-hoki"></i>
                    <div class="widget-thumb-body" style="float:left;margin-top: -55px;">
                        <input type="file" name="upload_file" style="float:left;">
                        <button type="submit" class="btn green btn-xs customactionbtn" style="background-color: #4CAF50 !important;border-color: #4CAF50 !important;border-radius: 3px !important;font-family: Roboto-Regular;margin-top: 8px;"><i class="fa fa-upload"></i> Upload</button>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <div class="row" id="recent">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase recent-activity">Recent Activities</span>
                    </div>             
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <ul class="feeds">
                            <li>
                                <a href="javascript:;">
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-success">
                                                    <i class="icon-envelope"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> Admin Added New Tender posted. </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="col2">
                                        <div class="date"> 20 mins </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-success">
                                                    <i class="icon-envelope"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> Admin Added New Tender posted. </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="col2">
                                        <div class="date"> 22 mins </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm bg-yellow-casablanca">
                                                <i class="icon-badge"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> Award Added This Tender002 Tender
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date"> 40 mins </div>
                                </div>
                            </li>
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm bg-yellow-casablanca">
                                                <i class="icon-badge"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> Just Now Award Added This Tender001 Tender
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date"> Just Now </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="scroller-footer">
                        <div class="btn-arrow-link pull-right">
                           <!--  <a href="javascript:;">See All Records</a>
                                <i class="icon-arrow-right"></i> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>