<?php
if(count($singlebiddingList)>0) {
   $part_no = $singlebiddingList["part_no"];
   $part_name = $singlebiddingList["part_name"];
   $ref_no = $singlebiddingList["ref_no"];
   $base_price = $singlebiddingList["base_price"];
   $quantity = $singlebiddingList["quantity"];
   $delivery_duration = $singlebiddingList["delivery_duration"];
   $start_date = date("d/m/Y",strtotime($singlebiddingList["start_date"]));
   $end_date = date("d/m/Y",strtotime($singlebiddingList["end_date"]));
   $description = $singlebiddingList["description"];
   $applyprice= $singlebiddingList["applyprice"];
    $vendor_id= $singlebiddingList["vendor_id"];
   

} else {
    $part_no= $part_name= $ref_no= $base_price= $quantity= $delivery_duration=$start_date= $end_date= $description= $applyprice= $vendor_id="";
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url()?>assets/layouts/layout/img/de-active/bitting.png" class="imgbasline">Supplier Can Change the Tender Price</div>
            <div class="tools">
                <!-- <a href="javascript:;" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                <a href="javascript:;" class="reload"> </a>
                <a href="javascript:;" class="remove"> </a> -->
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" action="" class="horizontal-form" method="POST">
                <input type="hidden" name="hnd_vendor_id" id="hnd_vendor_id" value="<?php echo $vendor_id ?>">
                <div class="form-body">
                    <h3 class="form-section formheading">Tender Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                            	<label class="control-label col-md-4">Part No</label>
                                <div class="col-md-8">
                                	<input type="text" class="form-control" name="part_no" id="part_no" placeholder="" value="<?php echo $part_no ?>" disabled>
                                </div>
                                
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_name" id="part_name" placeholder="" value="<?php echo $part_name ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                            	<label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="" value="<?php echo $ref_no ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Base Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="base_price" id="base_price" placeholder="" value="<?php echo $base_price ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                            	<label class="control-label col-md-4">Qunatity</label>
                                <div class="col-md-8"> 
                                     <input type="text" class="form-control" name="quantity" id="quantity" placeholder="" value="<?php echo $quantity ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_duration" id="delivery_duration" placeholder="Delivery Duration in Days" value="<?php echo $delivery_duration ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                     
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" value="<?php echo $start_date?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" value="<?php echo $end_date?>" disabled> 
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <h3 class="form-section formheading">Tender Detailed Description or Information</h3>
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="tender_detail" id="tender_detail" rows="5" cols="50" placeholder="Enter the Tender Details...." disabled><?php echo  $description ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <!-- <span class="help-block pull-right">(Upload Multiple Document's)</span> -->
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Terms and Condition</label>
                                <div class="col-md-5">
                                    <a href="<?php echo base_url()?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div> -->
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <!-- <span class="help-block pull-right">(Upload Multiple Document's)</span> -->
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <div class="col-md-5">
                                   
                                   <a href="<?php echo base_url()?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="helpmore" id="helpmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div> -->
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Detail</label>
                                <div class="col-md-5">
                                    <a href="<?php echo base_url()?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="detailmore" id="detailmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div> -->
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Documents Required</label>
                                <div class="col-md-5">

                                    <a href="<?php echo base_url()?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdocmore" id="tenderdocmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div> -->
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <a href="<?php echo base_url()?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoconemore" id="tenderdoconemore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div> -->
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <a href="<?php echo base_url()?>uploads/sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <!-- <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoctwomore" id="tenderdoctwomore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div> -->
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Price</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tender_price" id="tender_price" autocomplete="off" value="<?php echo $base_price ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">No of Qunatity</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="qunatity_no" id="qunatity_no" autocomplete="off" value="<?php echo $quantity ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter Your Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="your_price" id="your_price" autocomplete="off" value="<?php echo $applyprice ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                           
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Apply Tender
                    </button>
                    <!-- <a href="previewbidding" type="button" class="btn red customrestbtn" > <i class="fa fa-eye"></i> Preview Tender</a> -->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->