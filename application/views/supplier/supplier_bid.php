<?php
	$search_startdate = $search_enddate= $search_rfq = $search_tender = $search_part = '';
	if (!empty($search)) {
		$search_startdate = $search['start_date'];
		$search_enddate = $search['end_date'];
		$search_rfq = $search['rfq_no'];
		$search_tender = $search['tender_title'];
		$search_part = $search['part_name'];
	}
?>
<!-- <meta http-equiv="refresh" content="5"> -->
<!--BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    	$msg=$this->session->flashdata('message');
    if (!empty($msg)) {
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12 alertpadding">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url()?>assets/layouts/layout/img/de-active/bitting.png" class="imgbasline"> Supplier Bidding </div>
            <div class="actions">
                <!-- <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fa fa-plus"></i> Add </a>
                <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fa fa-print"></i> Print </a> -->
            </div>
        </div>
        <div class="portlet-body">

        	<form name="frm_bidding" id="frm_bidding" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                        	<input type="text" class="form-control" name="bidding[start_date]" id="start_date" autocomplete="off" Placeholder="Tender Start Date" value="<?php echo $search_startdate ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="bidding[end_date]" id="end_date" autocomplete="off" Placeholder="Tender End Date" value="<?php echo $search_enddate ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="bidding[rfq_no]" id="rfq_no" placeholder="RFQ No"  value="<?php echo $search_rfq ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="bidding[part_name]" id="part_name" placeholder="Part Name" value="<?php echo $search_part ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="bidding[tender_title]" id="tender_title" placeholder="Tender Title" value="<?php echo $search_tender ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."supplier/bidding";?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover" id="biddingtbl">
		            	<thead>
		                    <tr>
		                    	<th> SI.NO </th>
		                        <th> RFQ No</th>
		                        <th> E Quote Title</th>
		                        <th> Tender Base Price</th>
		                        <th> Quote Price</th>
		                        <th> Current Price</th>
		                        <th> Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    if(count($biddingGetList,COUNT_RECURSIVE)>1) {
		                    	$sno=1;
			                    foreach ($biddingGetList as $getbiddingData) {
			                    ?>
			                    <tr>
			                    	<td><?php echo $sno ?></td>
			                        <td><?php echo $getbiddingData["ref_no"] ?></td>
			                        <td><?php echo $getbiddingData["tender_title"]?></td>
			                        <td><?php echo $getbiddingData["base_price"] ?></td>
			                        <td><?php echo $getbiddingData["applyprice"]?></td>
			                        <td><?php echo $getbiddingData["currentPrice"]?></td>
			                        <?php
			                          if(strtolower($getbiddingData["viewbtn"]) == "show") {
			                        ?>
			                        <td><a href="bidding/changebidding/<?php echo $getbiddingData["tender_id"] ?>" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
			                        <?php
			                        } else {
			                        	echo "<td>-</td>";
			                        }
			                        ?> 
		                    	</tr>
			                    <?php
			                    $sno++;
			                    }
		                    }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY