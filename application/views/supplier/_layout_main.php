<?php 
  include("components/session_check.php");
  $this->load->view('supplier/components/header');
 ?>
<body>
    <div class="page-container">	            
        <?php $this->load->view('supplier/components/sidebar'); ?>	
        <div class="page-content-wrapper">
                <?php echo $subview ?>
        </div>

    </div>
    <?php $this->load->view('supplier/components/footer'); ?>     
