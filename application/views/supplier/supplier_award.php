
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url()?>assets/layouts/layout/img/de-active/awards.png" class="imgbasline"> Supplier Awards</div>
            <div class="actions">
                <!-- <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fa fa-plus"></i> Add </a>
                <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fa fa-print"></i> Print </a> -->
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="RFQ No">
                        </div>
	        		</div>
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder="Tender Name">
                        </div>
	        		</div>
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="award_date" id="award_date" placeholder="Awarded Start Date">
                        </div>
	        		</div>
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="award_enddate" id="award_enddate" placeholder="Awarded End Date">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover suppliertbl" id="award-tenders">
	            	<thead>
	                    <tr>
	                    	<th> SI.No </th>
	                        <th> RFQ NO </th>
	                        <th> Tender Name</th>
	                       <!--  <th> Vendor Name</th> -->
	                        <th> Awarded Date</th>
	                        <th> Awarded </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                    	<td>1</td>
	                        <td>TENDERADMIN-01</td>
	                        <td>Website Tender</td>
	                        <!-- <td>iStudio Technologies</td> -->
	                        <td>29/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>2</td>
	                        <td>TENDERADMIN-02</td>
	                        <td>Web Application</td>
	                        
	                        <td>28/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>3</td>
	                        <td>TENDERADMIN-01</td>
	                        <td>Network Tender</td>
	                        <td>28/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>4</td>
	                        <td>TENDERADMIN-03</td>
	                        <td>Admin Panel</td>
	                        <td>27/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>5</td>
	                        <td>TENDERADMIN-05</td>
	                        <td>Spare Parts</td>
                            
	                        <td>29/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>6</td>
	                        <td>TENDERADMIN-06</td>
	                        <td>Spare Parts1</td>
	                        
	                        <td>30/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>7</td>
	                        <td>TENDERADMIN-02</td>
	                        <td>Web Application</td>
	                        
	                        <td>25/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>8</td>
	                        <td>TENDERADMIN-07</td>
	                        <td>Network Switch</td>
	                        
	                        <td>26/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>9</td>
	                        <td>TENDERADMIN-03</td>
	                        <td>Admin Panel</td>
	                        
	                        <td>27/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>10</td>
	                        <td>TENDERADMIN-05</td>
	                        <td>Website Tender</td>
	                        <td>30/05/2018</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>