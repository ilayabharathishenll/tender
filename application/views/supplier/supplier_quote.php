<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url();?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> E Quote Details</div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover suppliertbl" id="supplier-quote">
	            	<thead>
	                    <tr>
	                    	<th> SI.No </th>
	                        <th> RFQ No </th>
	                        <th> E Quote Title</th>
	                        <th> Duration in Days </th>
	                        <th> E Quote End Date </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                 </tbody>
                 <?php
                  $i=1;
                  if(!empty($data)){
                  foreach ($data as $value) { ?>
                   <tr>
                         <td><?php echo $i; ?></td>
                         <td><?php echo $value->ref_no;?></td>
                         <td><?php echo $value->tender_title;?></td>
                         <td><?php echo $value->delivery_duration;?></td>
                         <td><?php echo $value->end_date;?></td>
                         <td><a href="<?php echo base_url('supplier/equote/change_tender_price');?>/<?php echo $value->tender_id;?>" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-edit"></i> View</a></td>
                      </tr>
                   
                 <?php $i++;}}
                  ?>
                     
                 </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>