<?php 
//print_r($data);exit;
foreach($data['tender_list'] as $tender){ ?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> View Tender Details</div>
            <div class="actions">
                <a href="tender_list" class="btn red btn-sm customrestbtn">
                    <i class="fa fa-angle-left"></i> Back</a>
            </div>
        </div>
        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="change_tender_price" id="change_tender_price" action="" class="horizontal-form" method="POST">
                <div class="form-body">
                    <h3 class="form-section formheading">Tender Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->ref_no)?$tender->ref_no:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->part_name)?$tender->part_name:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->delivery_duration)?$tender->delivery_duration:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->delivery_date)?date('d-m-Y',strtotime($tender->delivery_date)):"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Select Quantity</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->quantity)?$tender->quantity:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Base Price</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->base_price)?$tender->base_price:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Title</label>
                                <div class="col-md-8">
                                   <?php echo !empty($tender->tender_title)?$tender->tender_title:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spesc</label>
                                <div class="col-md-8">
                                 : <a href="<?php echo base_url('supplier/equote/download');?>/<?php echo !empty($tender->drawing_doc)?$tender->drawing_doc:"#"; ?>"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->norm)?$tender->norm:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price</label>
                                <div class="col-md-8">
                                     <?php echo !empty($tender->rm_price)?$tender->rm_price:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Details</label>
                                    <div class="col-md-8">
                                     <?php echo !empty($tender->tender_title)?$tender->tender_title:"--"; ?>detail
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload Tender Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Terms and Condition</label>
                                <?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='upload_terms'){ ?>
                                <div class="col-md-5">
                                    : <a href="<?php echo base_url('supplier/equote/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <?php } } ?>
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='upload_helpdoc'){ ?>
                                <div class="col-md-5">
                                    : <a href="<?php echo base_url('supplier/equote/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <?php } } ?>
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Detail</label>
                                <?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='upload_tenderdetail'){ ?>
                                <div class="col-md-5">
                                    : <a href="<?php echo base_url('supplier/equote/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <?php } } ?>
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Documents Required</label>
                                <?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='upload_tenderdoc'){ ?>
                                <div class="col-md-5">
                                    : <a href="<?php echo base_url('supplier/equote/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <?php } } ?>
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='tender_docone'){ ?>
                                <div class="col-md-5">
                                    : <a href="<?php echo base_url('supplier/equote/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <?php } } ?>
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='tender_doctwo'){ ?>
                                <div class="col-md-5">
                                    : <a href="<?php echo base_url('supplier/equote/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <?php } } ?>
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Date Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                     <?php echo !empty($tender->start_date)?date('d-m-Y',strtotime($tender->start_date)):"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                     <?php echo !empty($tender->end_date)?date('d-m-Y',strtotime($tender->end_date)):"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <h3 class="form-section formheading">Tender Price</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Price</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->base_price)?$tender->base_price:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">No of Qunatity</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->quantity)?$tender->quantity:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter Your Price</label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" name="price" id="price" autocomplete="off" value="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                           
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Apply Tender
                    </button>
                    <a href="preview_tender.php" type="button" class="btn red customrestbtn"> <i class="fa fa-eye"></i> Preview Tender</a>
                </div>
                <?php } ?>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>