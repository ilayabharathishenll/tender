<?php 
$this->load->view('admin/components/header'); 
include("components/session_check.php");
?>
<body>
    <div class="page-container">
        <?php $this->load->view('admin/components/sidebar'); ?>
        <div class="page-content-wrapper">
            <?php echo $subview ?>
        </div>
    </div>
    <?php $this->load->view('admin/components/footer'); ?>
