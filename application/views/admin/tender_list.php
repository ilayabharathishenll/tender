<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Admin Announced List of E Quote
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<form method="POST" action="<?php echo base_url('admin/tender/export_excel'); ?>">
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="Enter RFQ No">
                        </div>
	        		</div>
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <button type="submit"  class="btn red customrestbtn tendersearch" action><i class="fa fa-file-excel-o"></i> Export to Excel</button>
                        </div>
	        		</div>
	        		</form>
	        		<form method="POST" action="#">
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	        				<?php ?>
                           <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="RFQ No" value="<?php echo $this->input->get('no')!=''?$this->input->get('no'):'';?>">
                        </div>
	        		</div>
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="tender_name" id="tender_name" placeholder="Tender Name" value="<?php echo $this->input->get('title')!=''?$this->input->get('title'):'';?>">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="submit" class="btn btn-warning customsearchtbtn" name="search" value="search"> <i class="fa fa-search"></i> Search</button>
	        				<?php if($this->input->get('search')==1){ ?>
	        					<a href="<?php echo base_url('admin/tender/tender_list'); ?>" class="btn red customrestbtn"><i class="fa fa-refresh"></i> Reset</a>
	        				<?php }else{ ?>
	        					<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        				<?php } ?>
	        				
	        			</div>
	        		</div>
	        	</form>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover admintbl" id="tendertbl">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> RFQ NO </th>
	                        <th> Tender Name </th>
	                        <th> E Quote Name </th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                <?php
	                	$i=1;
	                	if(!empty($data)){
	                	foreach ($data['tender'] as $value) { ?>
	                		<tr>
	                        <td><?php echo $i; ?></td>
	                        <td><?php echo $value->ref_no;?></td>
	                        <td><?php echo !empty($value->tender_title)?$value->tender_title:"--";?></td>
	                        <td> 001-Web</td>
	                        <td><span class="label label-sm label-info labelboader"> Open </span> </td>
	                        <td> <a href="<?php echo base_url('admin/tender/edit_rfq');?>?id=<?php echo $value->tender_id;?>" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="<?php echo base_url('admin/tender/view_tender');?>?id=<?php echo $value->tender_id;?>" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        <a href="<?php echo base_url('admin/tender/delete');?>?id=<?php echo $value->tender_id;?>" type="button" class="btn red btn-xs customactionbtn"><i class="fa fa fa-trash"></i> Delete</a> </td>
	                    	</tr>
	                <?php $i++;}}
	                 ?>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>