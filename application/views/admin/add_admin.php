<?php
error_reporting(0);
if(count($getadmin)>0) {
    $admin_name=$getadmin["admin_name"];
    $admin_email=$getadmin["admin_email"];
    $admin_password=$getadmin["admin_password"];
    $admin_phone=$getadmin["admin_phone"];
    $admin_altphone=$getadmin["admin_altphone"];
} else {
    $admin_name="";
    $admin_email="";
    $admin_password="";
    $admin_phone="";
    $admin_altphone="";
}
?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/user.png" class="imgbasline"> Add Admin
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_addadmin" id="frm_addadmin" action="add_admin" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="<?php echo $getadmin["admin_id"] ?>">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="<?php echo $admin_name ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Email</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="" value="<?php echo $admin_email ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Password</label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="" value="<?php echo $admin_password ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Confirm Password</label>
                                <div class="col-md-8">
                                    <input type="Password" class="form-control" name="conpassword" id="conpassword" placeholder="" value="<?php echo $admin_password ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Phone</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="" value="<?php echo $admin_phone ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Alternate Phone No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="alt_phone" id="alt_phone" placeholder="" value="<?php echo $admin_altphone?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->