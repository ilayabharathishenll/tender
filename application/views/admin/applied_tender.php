<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/applied_tenders.png" class="imgbasline"> Applied Tender
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" Placeholder="Tender Start Date">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" Placeholder="Tender End Date">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="search_tender" id="search_tender" placeholder="Search Tender">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="RFQ No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="part_no" id="part_no" placeholder="Part No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder="Tender Title">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover admintbl" id="applied-tender">
	            	<thead>
	                    <tr>
	                    	<th> SI.NO </th>
	                        <th> RFQ NO </th>
	                        <th> Tender Name</th>
	                        <th> E Quote Name</th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                    	<td>1</td>
	                        <td>TENDERADMIN-01</td>
	                        <td>Website Tender</td>
	                        <td>001-Web</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>2</td>
	                        <td>TENDERADMIN-02</td>
							<td>Web Application</td>
	                        <td>002-Web</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>3</td>
	                        <td>TENDERADMIN-01</td>
	                        <td>Spare Parts1</td>
	                        <td>001-Parts</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>4</td>
	                        <td>TENDERADMIN-03</td>
	                        <td>Spare Parts</td>
	                        <td>003-Parts</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>5</td>
	                        <td>TENDERADMIN-05</td>
	                        <td>Network Tender</td>
                            <td>005-Network</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>6</td>
	                        <td>TENDERADMIN-06</td>
	                        <td>Admin Panel</td>
	                        <td>006-Panel</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>7</td>
	                        <td>TENDERADMIN-02</td>
	                        <td>Spare Parts</td>
	                        <td>002-Parts</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>8</td>
	                        <td>TENDERADMIN-07</td>
	                        <td>Spare Parts1</td>
	                        <td>007-Parts</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>9</td>
	                        <td>TENDERADMIN-03</td>
	                        <td>Website Tender</td>
	                        <td>003-Web</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>10</td>
	                        <td>TENDERADMIN-05</td>
	                        <td>Website Tender</td>
	                        <td>005-Web</td>
	                        <td><a href="applied_list" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
