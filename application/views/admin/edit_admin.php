<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-user"></i>Edit Admin
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_vendor" id="frm_vendor" action="admin_list" class="horizontal-form" method="POST">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="name" id="name" value="admin" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Email</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="email" id="email" value="admin@gmail.com" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Password</label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" name="password" id="password" value="123456" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Confirm Password</label>
                                <div class="col-md-8">
                                    <input type="Password" class="form-control" name="conpassword" id="conpassword" placeholder="" value="123456">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Phone</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="Phone" id="Phone" placeholder="" value="9039399393">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Alternate Phone No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="alt_phone" id="alt_phone" placeholder="" value="9039399392">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>