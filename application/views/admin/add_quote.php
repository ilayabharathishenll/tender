<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Admin Posting E Quote
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_quote" id="frm_quote" class="horizontal-form" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter the Part No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_no" id="part_no" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter the Part Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_name" id="part_name" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter the Part Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_price" id="part_price" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter the Qunatity</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="quantity" id="quantity" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Supplier can change the price details after the tender applied with in particular time of period</label>
                                <div class="col-md-8">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="period" class="checkempty" value="1" checked> Yes 
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="period" class="checkempty" value="2"> No 
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-3 drawinglabel">Enter the Drawing Issues No</label>
                                <div class="col-md-3 drawingtextbox">
                                    <input type="text" class="form-control" name="drawing_no" id="drawing_no" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>