<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/awards.png" class="imgbasline"> Manage Awards
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="RFQ No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder="Tender Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="vendor_name" id="vendor_name" placeholder="Vendor Name">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover suppliertbl" id="award-list">
	            	<thead>
	                    <tr> 
	                    	<th> SI.NO </th>
	                        <th> RFQ NO </th>
	                        <th> Tender Name</th>
	                        <th> Vendor Name</th>
	                        <th> Quote Price</th>
	                        <th> Awarded </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                    	<td>1</td>
	                        <td>TENDERADMIN-01</td>
	                        <td>Website Tender</td>
	                        <td>iMax Technologies</td>
	                        <td>10000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>2</td>
	                        <td>TENDERADMIN-02</td>
	                        <td>Web Application</td>
	                        <td>iStudio Technologies</td>
	                        <td>12000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>3</td>
	                        <td>TENDERADMIN-01</td>
	                        <td>Spare Parts</td>
	                        <td>SAP Technologies</td>
	                        <td>11000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>4</td>
	                        <td>TENDERADMIN-03</td>
	                        <td>Spare Parts1</td>
	                        
	                        <td>Swap Technologies</td>
	                        <td>90000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>5</td>
	                        <td>TENDERADMIN-05</td>
	                        <td>Website Tender</td>
	                        <td>iStudio Technologies</td>
	                        <td>10000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>6</td>
	                        <td>TENDERADMIN-06</td>
	                        <td>Network switches for BAE</td>
	                        <td>Network Technologies</td>
	                        <td>11000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>7</td>
	                        <td>TENDERADMIN-02</td>
	                        <td>Website Tender</td>
	                        <td>iStudio Technologies</td>
	                        <td>13000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>8</td>
	                        <td>TENDERADMIN-07</td>
	                        <td>Website Application</td>
	                        
	                        <td>Web Technologies</td>
	                        <td>12000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>9</td>
	                        <td>TENDERADMIN-03</td>
	                        <td>Website Tender</td>
	                        
	                        <td>iStudio Technologies</td>
	                        <td>17000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>10</td>
	                        <td>TENDERADMIN-05</td>
	                        <td>Website Tender</td>
	                        <td>iStudio Technologies</td>
	                        <td>16000</td>
	                        <td> 
	                        	<i class="icon-badge font-yellow-casablanca"></i>
	                        </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>