<?php
	$search_code = $search_name = $search_category = '';
	if (!empty($search)) {
		$search_code = $search['vendor_code'];
		$search_name = $search['vendor_name'];
		$search_category = $search['category'];
	}
?>
<div class="page-content">
	<div class="col-md-12 col-sm-12 col-xs-12 alertpadding" id="alertappend">
	</div>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/invitation.png" class="imgbasline"> Send Tender Invitation
            </div>
        </div>
        <div class="portlet-body">
	        
        	<form name="frm_sendinvite" id="frm_sendinvite" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="sendinvite[vendor_code]" id="vendor_code" placeholder="Vendor Code" value="<?php echo $search_code ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="sendinvite[vendor_name]" id="vendor_name" placeholder="Vendor Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                          <select class="form-control" name="sendinvite[category]" id="category">
	                          	<option value="">--Select Vendor Category--</option>
	                          	<?php
	                          	 	foreach ($catgoryData as $getCategory) {
	                          	 		$selected="";
	                          	 		if ($search_category==$getCategory->category_id) {
	                                       $selected="selected";
	                          	 		}
	                          	 		echo "<option value=".$getCategory->category_id." ".$selected.">".$getCategory->category_name."</option>";
	                          	 	}
	                          	?>
	                          </select>
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<button type="button" class="btn green customsavebtn" id="sendinvitemail"> <i class="fa fa-sign-in"></i> Send</button>
		        				<a href="<?php base_url()."admin/invite/send_invite/"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover admintbl" id="send-invite">
		            	<thead>
		                    <tr>
		                    	<th>
		                    		<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
	                                    <input type="checkbox" class="checkboxes" name="export_all" onclick="checkAllExp(this)">
	                                    <span></span>
	                                </label>
	                            </th>
	                            <th>SI.NO</th>
		                        <th>Vendor Code</th>
		                        <th>Vendor Name</th>
		                        <th>Vendor Category</th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    $sno=1;
		                    if (count($getVendorInvList,COUNT_RECURSIVE)>1) {
		                    	foreach($getVendorInvList as $sendinvite){
		                    ?>
			                    <tr>
			                    	<input type="hidden" name="hnd_tender" id="hnd_tender" value="<?php echo $sendinvite["tender_id"];?>">
			                        <td>
		                            	<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
		                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="<?php echo $sendinvite["vendor_id"];?>" class="checkboxes" onclick="eachChk()">
		                                    <span></span>
		                                </label>
			                        </td>
			                        <td><?php echo $sno ?></td>
			                        <td><?php echo $sendinvite["vendor_code"]; ?></td>
			                        <td><?php echo $sendinvite["vendor_name"]; ?></td>
			                        <td><?php echo $sendinvite["vendor_category"]; ?></td>
			                    </tr>
		                    <?php
		                      	$sno++;
		                    	}
		                	}
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>
