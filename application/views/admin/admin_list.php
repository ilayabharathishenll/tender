<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/user.png" class="imgbasline"> Admin List</div>
            <div class="actions">
                <a href="add_admin" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Admin</a>
            </div>
        </div>
        <div class="portlet-body">
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
	            	<thead>
	                    <tr>
	                        <th style="width: 50px;">SI.NO</th>
	                        <th>Name</th>
	                        <th>Email</th>
	                        <th>Phone</th>
	                        <th>Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <?php
	                    $sno=1;
	                    foreach ($adminList as $admindata) {
	                    	$adminid=$admindata->admin_id;
	                    ?>
	                    <tr>
	                        <td><?php echo $sno?></td>
	                        <td><?php echo $admindata->name; ?></td>
	                        <td><?php echo $admindata->email; ?></td>
	                        <td><?php echo $admindata->phone;?></td>
	                        <td> 
	                        	<?php 
	                        	  $admin_id=$this->session->userdata("admin_id");
	                        	  if($admin_id==$adminid) {
	                        	?>
	                        	<a href="add_admin/<?php echo $admindata->admin_id;?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <!-- <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a> -->
	                        	<?php
                                 } else {
                                 	echo "-";
                                 }
	                        	?>
	                        </td>
	                    </tr>
	                    <?php
	                      $sno++;
	                    }
	                    ?>
	                    <!-- <tr>
	                        <td> 2 </td>
	                        <td>John</td>
	                        <td>john@gmail.com </td>
	                        <td>9009389282</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td>David</td>
	                        <td>david@gmail.com </td>
	                        <td>9009389283</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td>Vicky</td>
	                        <td>vicky@gmail.com </td>
	                        <td>9009389284</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td>Ram</td>
	                        <td>ram@gmail.com </td>
	                        <td>9009389285</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 6 </td>
	                        <td>Ajay</td>
	                        <td>ajay@gmail.com </td>
	                        <td>9009389286</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 7 </td>
	                        <td>Admin</td>
	                        <td>admin7@gmail.com </td>
	                        <td>9009389287</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 8 </td>
	                        <td>Admin</td>
	                        <td>admin8@gmail.com </td>
	                        <td>9009389288</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 9 </td>
	                        <td>Admin</td>
	                        <td>admin9@gmail.com </td>
	                        <td>9009389289</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 10 </td>
	                        <td>Admin</td>
	                        <td>admin@gmail.com </td>
	                        <td>9009389290</td>
	                        <td> <a href="edit_admin" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr> -->
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>