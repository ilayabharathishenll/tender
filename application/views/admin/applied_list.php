<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/applied_tenders.png" class="imgbasline"> Applied Tenders
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="RFQ No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="vender_code" id="vender_code" placeholder="Vender Code">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="vendor_name" id="vendor_name" placeholder="Vendor Name">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="invitetbl">
	            	<thead>
	                    <tr>
	                    	<th> SI.NO </th>
	                        <th> RFQ NO </th>
	                        <th> Vender Code</th>
	                        <th> Vendor Name </th>
	                        <th> E Quote Base Price</th>
	                        <th> Quote Price </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                    	<td>1</td>
	                        <td>TENDERADMIN-01</td>
	                        <td>Vendor-01</td>
	                        <td>iStudio Technologies</td>
	                        <td> 100000 </td>
	                        <td> 94600 </td>
	                        <td>
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn green btn-xs customactionbtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>2</td>
	                        <td>TENDERADMIN-02</td>
	                        <td>Vendor-02</td>
	                        <td>iMax Technologies</td>
	                        <td> 120000 </td>
	                        <td> 94600 </td>
	                        <td> 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn green btn-xs customactionbtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>3</td>
	                        <td>TENDERADMIN-01</td>
	                        <td>Vendor-03</td>
	                        <td>SAP Technologies</td>
	                        <td> 100000 </td>
	                        <td> - </td>
	                        <td> 
	                        	 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>4</td>
	                        <td>TENDERADMIN-03</td>
	                        <td>Vendor-04</td>
	                        <td>TVS Technologies</td>
	                        <td> 100000 </td>
	                        <td> - </td>
	                        <td> 
	                        	 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>5</td>
	                        <td>TENDERADMIN-05</td>
	                        <td>Vendor-05</td>
	                        <td>iStudio Technologies</td>
	                        <td> 100000 </td>
	                        <td> - </td>
	                        <td> 
	                        	 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>6</td>
	                        <td>TENDERADMIN-06</td>
	                        <td>Vendor-06</td>
	                        <td>Swap Technologies</td>
	                        <td> 100000 </td>
	                        <td> 94600 </td>
	                        <td> 
	                        	 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn green btn-xs customactionbtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>7</td>
	                        <td>TENDERADMIN-02</td>
	                        <td>Vendor-06</td>
	                        <td>iStudio Technologies</td>
	                        <td> 100000 </td>
	                        <td> 94600 </td>
	                        <td> 
	                        	 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn green btn-xs customactionbtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>8</td>
	                        <td>TENDERADMIN-07</td>
	                        <td>Vendor-07</td>
	                        <td>IMax Technologies</td>
	                        <td> 100000 </td>
	                        <td> 94600 </td>
	                        <td> 
	                        	 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn green btn-xs customactionbtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>9</td>
	                        <td>TENDERADMIN-03</td>
	                        <td>Vendor-08</td>
	                        <td>iStudio Technologies</td>
	                        <td> 100000 </td>
	                        <td> 94600 </td>
	                        <td> 
	                        	 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn green btn-xs customactionbtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>10</td>
	                        <td>TENDERADMIN-05</td>
	                        <td>Vendor-10</td>
	                        <td>iStudio Technologies</td>
	                        <td> 100000 </td>
	                        <td> 94600 </td>
	                        <td> 
	                        	 
	                        	<a href="<?php echo base_url().'admin/awards/award_list'; ?>" type="button" class="btn green btn-xs customactionbtn"> <i class="icon-badge"></i> Award</a>
	                        </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
	$('#invitetbl').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();   
</script>