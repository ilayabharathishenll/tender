<?php 
//print_r($data);
foreach($data['tender_list'] as $tender){ ?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> RFQ Details
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" action="#" class="horizontal-form" method="POST">
                <div class="form-body">
                    <h3 class="form-section formheading">Tender Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="" value=" <?php echo !empty($tender->ref_no)?$tender->ref_no:"--"; ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="sel_partname" id="sel_partname">
                                        <option value="">----Select Part Name----</option>
                                        <option value="1" selected>Part one</option>
                                        <option value="2">Part two</option>
                                        <option value="3">Part three</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_duration" id="delivery_duration" placeholder="Delivery Duration in Days" value="<?php echo !empty($tender->delivery_duration)?$tender->delivery_duration:"--"; ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_date" id="delivery_date" placeholder="" value="<?php echo !empty($tender->delivery_date)?date('d/m/Y',strtotime($tender->delivery_date)):"--"; ?>" data-date-format="dd/mm/yyyy">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Select Qunatity</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="sel_qunatity" id="sel_qunatity">
                                        <option value="">----Select Quantity----</option>
                                        <option value="1">1</option>
                                        <option value="2" selected>2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Base Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="base_price" id="base_price" placeholder="" value="<?php echo !empty($tender->base_price)?$tender->base_price:"--"; ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Title</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder="" value="<?php echo !empty($tender->tender_title)?$tender->tender_title:"--"; ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spesc</label>
                                <div class="col-md-8">
                                    <input type="file" name="upload_drawing" id="upload_drawing" value="<?php echo $_SERVER['DOCUMENT_ROOT']."".str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']); ?>uploads/<?php echo $tender->drawing_doc;?>">
                                   
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="norm" id="norm" placeholder="" value="<?php echo !empty($tender->norm)?$tender->norm:"--"; ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rm_price" id="rm_price" placeholder="" value="<?php echo !empty($tender->rm_price)?$tender->rm_price:"--"; ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="tender_detail" id="tender_detail" rows="5" cols="50" placeholder="Enter the Tender Details...."><?php echo !empty($tender->tender_detail)?$tender->tender_detail:""; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload Tender Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Terms and Condition</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_terms[]" id="upload_terms">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <div class="col-md-5">
                                   
                                    <input type="file" name="upload_helpdoc[]" id="upload_helpdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="helpmore" id="helpmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Detail</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_tenderdetail[]" id="upload_tenderdetail">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="detailmore" id="detailmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Documents Required</label>
                                <div class="col-md-5">

                                    <input type="file" name="upload_tenderdoc[]" id="upload_tenderdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdocmore" id="tenderdocmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <input type="file"  name="tender_docone[]" id="tender_docone">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoconemore" id="tenderdoconemore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <input type="file" name="tender_doctwo[]" id="tender_doctwo">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoctwomore" id="tenderdoctwomore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Date Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" value="<?php echo !empty($tender->start_date)?date('d/m/Y',strtotime($tender->start_date)):"--"; ?>" data-date-format="dd/mm/yyyy">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" value="<?php echo !empty($tender->end_date)?date('d/m/Y',strtotime($tender->end_date)):"--"; ?>" data-date-format="dd/mm/yyyy">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" class="btn green customsavebtn" name="submit" value="submit">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php } ?>
<!-- END CONTENT BODY -->