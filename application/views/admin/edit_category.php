<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Edit Category
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="add_category" id="add_category" class="horizontal-form" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Category Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="category_name" id="category_name" placeholder="" value="<?php echo $edit_category->category_name;?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Category Code</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="cat_number" id="cat_number" placeholder="" value="<?php echo $edit_category->cat_number;?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>