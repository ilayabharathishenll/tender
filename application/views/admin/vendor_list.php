<?php
	$search_name = $search_email = $search_num = '';
	if (!empty($search)) {
		$search_name = $search['first_name'];
		$search_email = $search['email'];
		$search_num = $search['phone'];
	}
?>
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message');
    if(!empty($msg)) {
    ?>  
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/vendors.png" class="imgbasline"> Vendor List
            </div>
            <div class="actions">
                <a href="add_vendor" class="btn green btn-sm customaddbtn">
                    <i class="fa fa-plus"></i> Invite Vendor</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form id="vendor_list_search" method="post" class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="vendor[first_name]" id="vendor_name" placeholder="Vendor Name" value="<?php echo $search_name;?>">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="vendor[email]" id="email" placeholder="Email" value="<?php echo $search_email;?>">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="vendor[phone]" id="phone" placeholder="Contact Number" value="<?php echo $search_num;?>">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        			</div>
	        		</div>
	        	</div>
	        </form>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover admintbl" id="vendor-list">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Vendor Code</th>
	                        <th> Vendor Name </th>
	                        <th> Email </th>
	                        <th> Contact Number</th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                	<?php 
	                		if (!empty($vendor_list)) {
	                			$i = 1;
	                			foreach ($vendor_list as $vendor) {
	                				$status = $vendor->status; ?>
	                				<tr>
		                				<td> <?php echo $i?> </td>
										<td> Vendor-<?php echo $vendor->vendor_id; ?> </td>
										<td> <?php echo $vendor->first_name; ?> </td>
										<td> <?php echo $vendor->email; ?> </td>
		                				<td> <?php echo $vendor->phone; ?> </td>
		                				<?php if($status == 'Active'){?>
		                					<td><span class="label label-sm label-info labelboader">Active</span></td>
		                					<td style="text-align:left;" class="action"><a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" data-value="Trash" data-id="<?php echo $vendor->vendor_id; ?>" title="Delete"><i class="fa fa-trash"></i> Delete</a> <a href="javascript:void(0);" type="button" class="btn green btn-xs customactionbtn documentDownload" data-toggle="modal" data-value="view" data-id="<?php echo $vendor->vendor_id; ?>" data-target="#docPopup" title="View"><i class="fa fa-eye"></i> View</a></td>
	                					<?php } else if ($status == 'Inactive'){?>
	                						<td><span class="label label-sm label-danger labelboader">Inactive</span></td>
	                						<td style="text-align:left;" class="action"><a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" data-value="Trash" data-id="<?php echo $vendor->vendor_id; ?>" title="Delete" ><i class="fa fa-trash"></i> Delete</a> <a href="javascript:void(0);" type="button" class="btn green btn-xs customactionbtn documentDownload" data-toggle="modal" data-value="view" data-id="<?php echo $vendor->vendor_id; ?>" data-target="#docPopup" title="View"><i class="fa fa-eye"></i> View</a></td>
	                					<?php } else if ($status == 'Trash') { ?>
	                						<td><span class="label label-sm label-warning labelboader">Trash</span></td>
	                						<td style="text-align:left;" class="action"> <a href="javascript:void(0);" data-value="Inactive" data-id="<?php echo $vendor->vendor_id; ?>" type="button" class="btn green btn-xs customactionbtn" title="Restore"><i class="fa fa-floppy-o"></i> Restore</a></td>
                						<?php } else { ?>
                							<td><span class="label label-sm label-warning labelboader">Pending</span></td>
	                						<td style="text-align:left;" class="action"> <a href="javascript:void(0);" data-value="Active" data-id="<?php echo $vendor->vendor_id; ?>" type="button" class="btn green btn-xs customactionbtn" title="Active"><i class="fa fa-check"></i> Activate</a></td>
	                					<?php } ?>
	                				</tr>
	                			<?php 
	                				$i++;
	                			}
	                		}
                		?>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<div class="modal fade in" id="docPopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form class="vendor_status" id="vendor_status">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                <h4 class="modal-title"><b>Vendor Document</b></h4>
	            </div>
	            <div class="modal-body"> 
		            <h4 style="font-size:14px;"><b>Vendor Name:</b> <span></span></h4>
		            <div class="row">
		             	<div class="col-md-11" style="margin-top: 15px;">
		             		<div class="form-group">
		                        <label class="control-label col-md-4">Vendor Document:</label>
		                        <div class="col-md-8 vendor-doc">
		                            <a href="#"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 50px;"></i></a>
		                        </div>
		                    </div>
		             	</div>
		             	<div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
		             		<div class="form-group">
		                        <label class="control-label col-md-4">Vendor Status:</label>
		                        <div class="col-md-8 vendor-detail">
		                        	<input type="hidden" name="vid">
		                            <select class="form-control" name="status" id="status">
		                            	<option>--Select Status--</option>
		                            	<option value="Pending">Pending</option>
		                            	<option value="Active">Active</option>
		                            	<option value="Inactive">Inactive</option>
		                            	<option value="Trash">Trash</option>
		                            </select>
		                        </div>
		                    </div>
		             	</div>
		            </div>

		            <div class="modal-footer">
		            	<button type="submit" class="btn green customaddbtn"> <i class="fa fa-check"></i> Save</button>
		            	<button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
	                </div>
	            </div>
            </form>
        <!-- /.modal-content -->
       </div>
    <!-- /.modal-dialog -->
   </div>
</div>
<!-- END CONTENT BODY -->
