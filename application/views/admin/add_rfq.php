<!-- BEGIN CONTENT BODY -->
<?php foreach($data as $value){ ?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Adding RFQ Details
            </div>
        </div>
        <div class="portlet-body form">
            <?php //echo form_open_multipart('admin/tender/upload_file'); ?>
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" class="horizontal-form" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <h3 class="form-section formheading">Tender Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="" value="TENDER-<?php echo date('Y').'_'.sprintf("%03d", ($value->tender_id));?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="sel_partname" id="sel_partname" value="<?php echo !empty($value->part_name)?$value->part_name:"";?>" readonly>                                  
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_duration" id="delivery_duration" placeholder="Delivery Duration in Days">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_date" id="delivery_date" autocomplete="off" data-date-format="dd/mm/yyyy">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Quantity</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="sel_quantity" id="sel_quantity" value="<?php echo !empty($value->quantity)?$value->quantity:"0";?>" readonly>
                                  
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Base Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="base_price" id="base_price" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Title</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spesc</label>
                                <div class="col-md-8">
                                    <input type="file" name="upload_drawing" id="upload_drawing">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="norm" id="norm" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rm_price" id="rm_price" placeholder="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="tender_detail" id="tender_detail" rows="5" cols="50" placeholder="Enter the Tender Details...."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload Tender Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Terms and Condition</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_terms[]" id="upload_terms">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <div class="col-md-5">
                                   
                                    <input type="file" name="upload_helpdoc[0]" id="upload_helpdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="helpmore" id="helpmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Detail</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_tenderdetail[0]" id="upload_tenderdetail">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="detailmore" id="detailmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Documents Required</label>
                                <div class="col-md-5">

                                    <input type="file" name="upload_tenderdoc[0]" id="upload_tenderdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdocmore" id="tenderdocmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <input type="file"  name="tender_docone[0]" id="tender_docone">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoconemore" id="tenderdoconemore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <input type="file" name="tender_doctwo[0]" id="tender_doctwo">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoctwomore" id="tenderdoctwomore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Date Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" data-date-format="dd/mm/yyyy" >
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" data-date-format="dd/mm/yyyy">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php } ?>
<!-- END CONTENT BODY -->