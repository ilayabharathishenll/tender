<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> View Tender Details</div>
            <div class="actions">
                <a href="<?php echo base_url(); ?>admin/invite/tender_invite" class="btn red btn-sm customrestbtn">
                    <i class="fa fa-angle-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" action="tender_invite" class="horizontal-form" method="POST">
                <div class="form-body">
                    <h3 class="form-section formheading">Tender Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["ref_no"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["part_name"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["delivery_duration"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["delivery_date"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Select Qunatity</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["quantity"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Base Price</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["base_price"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Title</label>
                                <div class="col-md-8">
                                   : <?php echo $viewTender["tender_title"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spes</label>
                                <div class="col-md-8">
                                 : <a href="sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm</label>
                                <div class="col-md-8">
                                   : <?php echo $viewTender["norm"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price</label>
                                <div class="col-md-8">
                                   : <?php echo $viewTender["rm_price"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-12">Tender Details:</label>
                                    <div class="col-md-12">
                                    <?php echo $viewTender["tender_detail"];?>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload Tender Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Terms and Condition</label>
                                <div class="col-md-5">
                                    : <a href="sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                   
                                </div>
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <div class="col-md-5">
                                    : <a href="sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Detail</label>
                                <div class="col-md-5">
                                    : <a href="sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                    
                                </div>
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Documents Required</label>
                                <div class="col-md-5">
                                : <a href="sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                   
                                </div>
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    : <a href="sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                     
                                </div>
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    : <a href="sample.doc"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;"></i></a>
                                </div>
                                <div class="col-md-2">
                                   
                                </div>
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Date Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                   : <?php echo $viewTender["start_date"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["end_date"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>