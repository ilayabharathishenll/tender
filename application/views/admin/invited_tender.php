<?php
	$search_rfq = $search_tender = $search_part = '';
	if (!empty($search)) {
		$search_rfq = $search['rfq_no'];
		$search_tender = $search['tender_title'];
		$search_part = $search['part_name'];
	}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/invitation.png" class="imgbasline"> Invited Tender
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_invitelist" id="frm_invitelist" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="invite[rfq_no]" id="rfq_no" placeholder="RFQ No" value="<?php echo $search_rfq ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="invite[tender_title]" id="tender_title" placeholder="Tender Name" value="<?php echo $search_tender ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="invite[part_name]" id="part_name" placeholder="Part Name" value="<?php echo $search_part ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php base_url()."admin/invite/invited_tender"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover admintbl" id="invitetbl">
		            	<thead>
		                    <tr>
		                    	<th> SI.NO </th>
		                        <th> RFQ NO </th>
		                        <th> Tender Name</th>
		                        <th> Part Name</th>
		                        <th> Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                       $sno=1;
		                       foreach ($gettenderInvList as $singleTender) {
		                    ?>
		                    <tr>
		                    	<td><?php echo $sno; ?></td>
		                        <td><?php echo $singleTender->ref_no;?></td>
		                        <td><?php echo $singleTender->tender_title;?></td>
		                        <td><?php echo $singleTender->part_name;?></td>
		                        <td> 
		                        	<a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs custominvitebtn  sendinvitepopup" data-id="<?php echo $singleTender->tender_id;?>" data-rfq="<?php echo $singleTender->ref_no;?>"><i class="fa fa-envelope-o"></i> Invited Tender</a>
		                        </td>
		                    </tr>
		                    <?php
		                    	$sno++;
		                    }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>
<div class="modal fade in" id="sendPopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><b>Invited List</b></h4>
            </div>
            <div class="modal-body"> 
             <h4 style="font-size:14px;margin-top: -4px;"><b>RFQ No:</b> <span id="rfqapp"></span></h4>
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="invitetbl">
            	<thead>
            		<tr>
	            		<th>SI.No</th>
	            		<th>Vendor Code</th>
	            		<th>Vender Name</th>
	            		<th>Vender Product</th>
	            		<th>Status</th>
	            	</tr>
            	</thead>
            	<tbody id="inviteappendtable">
            		
            	</tbody>
            </table>
            </div>

            </div>
            <div class="modal-footer">
            	<button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
                </div>
            </div>
       </div>
   </div>
</div>