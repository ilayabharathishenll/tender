<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Bidding extends Supplier_Controller {

	public function index()
	{
		$data = array();
		if (!empty($_POST['bidding'])) {
			$data['search'] = $_POST['bidding'];
		}
		$this->load->model("Bidding_Model");
		$data['biddingGetList']= $this->Bidding_Model->getbiddingList();
		$data['subview'] = $this->load->view('supplier/supplier_bid', $data, TRUE);
		$this->load->view('supplier/_layout_main', $data);
	}
	public function changebidding()
	{
		$data = array();
		$tenderID= $this->uri->segment(4);
		$vendor_id=$this->input->post("hnd_vendor_id");
		if(!empty($vendor_id)) {
            $this->load->model("Bidding_Model");
			$data= $this->Bidding_Model->upadtebidding($vendor_id,$tenderID);
			if ($data=="success") {
				$this->session->set_flashdata('message', 'Bidding updated successfully.');
                redirect("supplier/bidding");
			}
		} else {
			$this->load->model("Bidding_Model");
			$data['singlebiddingList']= $this->Bidding_Model->changebidding($tenderID);
			$data['subview'] = $this->load->view('supplier/change_bid', $data, TRUE);
			$this->load->view('supplier/_layout_main', $data);
		}
	}
	public function previewbidding()
	{
		$data = array();
		$this->load->model("Bidding_Model");
		$data['subview'] = $this->load->view('supplier/preview_changebid', $data, TRUE);
		$this->load->view('supplier/_layout_main', $data);
	}
}