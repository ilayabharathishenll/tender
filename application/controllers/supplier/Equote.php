<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Equote extends Supplier_Controller {

	public function index()
	{
		$data = array();
		$data=$this->supplier_model->tender_list();
		$data['subview'] = $this->load->view('supplier/supplier_quote', array('data'=>$data), TRUE);
		$this->load->view('supplier/_layout_main', $data);
	}
	// public function single_quote()
	// {
	// 	$data = array();
	// 	$data['subview'] = $this->load->view('supplier/individual_quote', $data, TRUE);
	// 	$this->load->view('supplier/_layout_main', $data);
	// }
	public function change_tender_price($tender_id)
	{
		$data = array();
		$vendor_id=1;
		$data=$this->supplier_model->view_list($tender_id);
		if($this->input->post('submit') !=''){
			$datas['price']=$this->input->post("price");
			$datas['created_by']=1;
			$datas['modified_by']=1;
			$datas['vendor_id']=$vendor_id;
			$datas['tender_id']=$tender_id;
			//$datas['created_on']=date('Y-m-d H:i:s');
			$datas['created_date']=date('Y-m-d H:i:s');
			$inserted_id=$this->supplier_model->add($datas);
			$url_id=$inserted_id;
			$url="?id=".$url_id;
			redirect('supplier/equote/applied_tenders_list'.$url);
		}
		$data['subview'] = $this->load->view('supplier/change_tender_price', array('data'=>$data), TRUE);
		$this->load->view('supplier/_layout_main', $data);
		
	}
	public function applied_tenders_list()
	{
		$data = array();
		$data=$this->supplier_model->applied_tenders_list();
		$data['subview'] = $this->load->view('supplier/applied_tenders_list', array('data'=>$data), TRUE);
		$this->load->view('supplier/_layout_main', $data);
	}
	public function download($name=null){
    	force_download('./uploads/'.$name, NULL);
    }
}