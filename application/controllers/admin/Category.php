<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Category extends Admin_Controller {

    public function category_list()
	{
		$data = array();
		$data=$this->Category_Model->cat_list();
		$data['subview'] = $this->load->view('admin/category_list', array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
    }
    
	public function add_category()
	{
		$data = array();
		if($this->input->post('submit')){
			$data['category_name']=$this->input->post("category_name");
			$data['cat_number'] = $this->input->post("cat_number");
			$data['created_by']=1;
			$data['created_on']=date('Y-m-d H:i:s');
			$data['modified_by']=1;
			$data['modified_on']=date('Y-m-d H:i:s');
			$inserted_id=$this->Category_Model->add($data);
			redirect('admin/category/category_list');
		}
		else{
			$data['subview'] = $this->load->view('admin/add_category', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
		}
	}
	public function delete_category($category_id) {
        if ($category_id != '') {
            $this->Category_Model->delete($category_id);
            //$this->session->set_flashdata('success', 'Agent deleted successfully');
            redirect("admin/category/category_list");
        } else {
            //$this->session->set_flashdata('error', 'Please try again');
            redirect("admin/category/category_list");
        }
	}
	public function edit_category($category_id)	{
		$data = array();
	   $edit_category = $this->Category_Model->get_category($category_id);
	   if ($this->input->post("submit")) {
		$data['category_name']=$this->input->post("category_name");
		$data['cat_number'] = $this->input->post("cat_number");
		$data['status'] = 'Active';
		$data['modified_by']=1;
		$data['modified_on']=date('Y-m-d H:i:s');
	   	$check_cat = $this->Category_Model->check_cat($data['category_name'], $category_id);
		   if ($check_cat) {
				   $this->Category_Model->update($data, $category_id);
				   redirect("admin/category/category_list/" . $category_id);
				   //$this->session->set_flashdata('success', 'Agent updated successfully');
			   }
		   else {
			   //$this->session->set_flashdata('error', 'Agent name already exists');
			   redirect("admin/category/category_list/" . $category_id);
		   }
   		}
   		$data['subview'] =  $this->load->view('admin/edit_category', array('edit_category' => $edit_category[0]), TRUE);
   		$this->load->view('admin/_layout_main', $data);
   	}
}
