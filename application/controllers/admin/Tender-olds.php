<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Tender extends Admin_Controller {

	public function add_quote()
	{
		$data = array();
		if($this->input->post('submit')){
			$data['part_no']=$this->input->post("part_no");
			$data['part_name']=$this->input->post("part_name");
			$data['part_price']=$this->input->post("part_price");
			$data['quantity']=$this->input->post("quantity");
			$data['price_edit']=$this->input->post("period");
			$data['drawing_no']=$this->input->post("drawing_no");
			$data['created_by']=1;
			$data['modified_by']=1;
			$data['created_on']=date('Y-m-d H:i:s');
			$data['modified_on']=date('Y-m-d H:i:s');
			//print_r($data);exit;
			$inserted_id=$this->tender_model->add($data);
			//$url_id=$this->encrypt->encode($inserted_id);
			$url_id=$inserted_id;
			$url="?id=".$url_id;
			redirect('admin/tender_add'.$url);
		}
		else{
			$data['subview'] = $this->load->view('admin/add_quote', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
		}
	}

	public function add_rfq()
	{
		$data = array();$tender_id='';
		if($this->input->get('id')!=''){
			$id=$this->input->get("id");
			$result=$this->tender_model->list_tender($id);
			//print_r($result);exit;
			if($this->input->post('submit')!=''){
		    	$data["ref_no"]=$this->input->post("rfq_no");
		    	$data['part_no']=$result[0]->part_no;
				$data['part_name']=$result[0]->part_name;
				$data['part_price']=$result[0]->part_price;
				$data['quantity']=$result[0]->quantity;
				$data['price_edit']=$result[0]->price_edit;
				$data['drawing_no']=$result[0]->drawing_no;
			    //$data["sel_part_name"]=$this->input->post("sel_partname"); 
			    $data["delivery_duration"]=$this->input->post("delivery_duration");
			    $data["delivery_date"]=date('Y-m-d',strtotime($this->input->post("delivery_date")));
			    $data["quantity"]=$this->input->post("sel_quantity");
			    $data["base_price"]=$this->input->post("base_price");
			    $data["tender_title"]=$this->input->post("tender_title");
			    $data["norm"]=$this->input->post("norm");
			    $data["rm_price"]=$this->input->post("rm_price");
			    //$data["price_edit"]=$this->input->post("period");
			    $data["tender_detail"]=$this->input->post("tender_detail");
			    $data["start_date"]=date('Y-m-d',strtotime($this->input->post("start_date")));
			    $data["end_date"]=date('Y-m-d',strtotime($this->input->post("end_date")));
			    $data['modified_by']=$id;
				$data['modified_on']=date('Y-m-d H:i:s');
				//print_r($data);exit;
				if($_FILES){
					$this->do_upload($_FILES,$id);
				}			    
				// $check = getimagesize($_FILES["upload_drawing"]["tmp_name"]);
	   //  		if($check !== false){
			 //        $image = $_FILES['upload_drawing']['tmp_name'];
			 //        $data['drawing_doc'] = addslashes(file_get_contents($image));
		  //   	}
				$output=$this->tender_model->update($id,$data);
				if(!empty($output)){
					redirect('admin/tender/tender_list');
				}    
			}
			else{
				$data['subview'] = $this->load->view('admin/add_rfq',array('data'=>$result), TRUE);
				$this->load->view('admin/_layout_main', $data);
			}
		}
		else{
			redirect('admin/dashboard');
		}
	}

	public function edit_rfq()
	{
		$data = array();
		if($this->input->get('id')){
			$data=$this->tender_model->view_list($this->input->get('id'));
		}
		//print_r($_POST);exit;
		if($this->input->post('submit')!=''){
			$id=$this->input->get('id');
			$datas['part_no']=$this->input->post("part_no");
			$datas['part_name']=$this->input->post("part_name");
			$datas['part_price']=$this->input->post("part_price");
			$datas['quantity']=$this->input->post("quantity");
			$datas['price_edit']=$this->input->post("period");
			$datas['drawing_no']=$this->input->post("drawing_no");
			$datas["ref_no"]=$this->input->post("rfq_no");
		    $datas["part_name"]=$this->input->post("sel_partname"); 
		    $datas["delivery_duration"]=$this->input->post("delivery_duration");
		    $datas["delivery_date"]=date('Y-m-d',strtotime($this->input->post("delivery_date")));
		    $datas["quantity"]=$this->input->post("sel_quantity");
		    $datas["base_price"]=$this->input->post("base_price");
		    $datas["tender_title"]=$this->input->post("tender_title");
		    $datas["norm"]=$this->input->post("norm");
		    $datas["rm_price"]=$this->input->post("rm_price");
		    //$data["price_edit"]=$this->input->post("period");
		    $datas["tender_detail"]=$this->input->post("tender_detail");
		    $datas["start_date"]=date('Y-m-d',strtotime($this->input->post("start_date")));
		    $datas["end_date"]=date('Y-m-d',strtotime($this->input->post("end_date")));
		    $datas['modified_by']='3';
			$datas['modified_on']=date('Y-m-d H:i:s');
			if($_FILES){
				$this->do_upload($_FILES,$id);
			}
			$output=$this->tender_model->update($id,$datas);
			if(!empty($output)){
				redirect('admin/tender/tender_list');
			}   
		}
		$data['subview'] = $this->load->view('admin/edit_rfq', array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function view_tender()
	{
		$data = array();
		if($this->input->get('id')){
			$data=$this->tender_model->view_list($this->input->get('id'));
		}
		$data['subview'] = $this->load->view('admin/view_tender', array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function tender_list()
	{
		$data=array();
		if($this->input->post('search')!=''){
			$ref_no=$this->input->post("rfq_no");
			$tender_title=$this->input->post("tender_name"); 
			$data =$this->tender_model->tender_list();
    		$url="?no=".$ref_no."&title=".$tender_title."&search=1";
			redirect('admin/tender/tender_list'.$url);
    	}
    	if($this->input->get('search')==1){
    		$datas["ref_no"]=$this->input->get("no");
			$datas["tender_title"]=$this->input->get("title");
			$data['tender'] =$this->tender_model->tender_list($datas); 

    	}else{
    		$data['tender'] =$this->tender_model->tender_list(); 
    	}
    	$data['category']=$this->category_model->cat_list();
    	//print_r($data);exit;
		$data['subview'] = $this->load->view('admin/tender_list', array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function applied_list()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/applied_list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function applied_tender()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/applied_tender', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function do_upload($files,$id){
	    $key=array_keys($files);
	    $this->upload->initialize($this->set_function());
	    foreach ($key as $value) {
	    	//echo $value;
	    	if((is_array($_FILES[$value]['size'])) && ($_FILES[$value]['size'][0]>0)){
	    		 for($i=0; $i< count($_FILES[$value]['name']); $i++)
			    {           
			        $_FILES['new']['name']= $files[$value]['name'][$i];
			        $_FILES['new']['type']= $files[$value]['type'][$i];
			        $_FILES['new']['tmp_name']= $files[$value]['tmp_name'][$i];
			        $_FILES['new']['error']= $files[$value]['error'][$i];
			        $_FILES['new']['size']= $files[$value]['size'][$i];  
			        if ( ! $this->upload->do_upload('new'))
	                {
                        $error = array('error' => $this->upload->display_errors());
                        $data['subview'] = $this->load->view('admin/add_rfq', $error, TRUE);
						$this->load->view('admin/_layout_main', $data);
	                }
	                else
	                {
                       $datas = array('upload_data' => $this->upload->data());
	                    $details["tender_id"]=$id;
					    $details["t_doc_type"]=$value;
					    $details["t_doc_name"]=$datas['upload_data']['file_name'];
					    $details["t_doc_file_name"]=$datas['upload_data']['file_name']."-".date('Y-m-d');
					    $details["status"]="Active";
					    $details['created_on']=date('Y-m-d H:i:s');
						$details['modified_date']=date('Y-m-d H:i:s');
						$details['created_by']=$id;
						$this->tender_model->add_details($details);
            		}
				}
	    	}
	    	elseif($_FILES[$value]['size']>0){

		        if ( ! $this->upload->do_upload($value))
                {
                    $error = array('error' => $this->upload->display_errors());
                    $data['subview'] = $this->load->view('admin/add_rfq', $error, TRUE);
					$this->load->view('admin/_layout_main', $data);
                }
                else
                {
                    $datas = array('upload_data' => $this->upload->data());
                    $data['drawing_doc']= $datas['upload_data']['file_name'];
                    $this->tender_model->update($id,$data);
        		}
		        
	    	}
	    }
	}
    private function set_function(){
    	$config = array();
	    $config['upload_path']  = './uploads/';
        $config['allowed_types']= '*';
        $config['max_size'] = 1000;
        $config['max_width'] = 1300;
        $config['max_height'] = 1024;
	    $config['overwrite'] = TRUE;
	    //$config['encrypt_name'] = TRUE;
	    return $config;
    }

    public function download($name=null){
    	force_download('./uploads/'.$name, NULL);
    }

    public function export_excel(){
       	$this->load->library("excel");  

       	if($this->input->post('rfq_no')!=''){

        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);

        $table_columns = array("S.No", "Ref No","Tender Title","Part No","Part Name","Part Price", "Drawing No","Delivery Duration","Delivery Date","Quantity", "Base Price","Norm", "Start Date", "End Date", "Price", "RM Price");

        $column = 0;
        $object->getActiveSheet()->setTitle('RFQ worksheet');  
        $datas['ref_no']=$this->input->post('rfq_no');
        if(!empty($datas)){
        	$data = $this->tender_model->tender_list($datas);
        
	          foreach($table_columns as $field)
	          {
	           $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	           $column++;
	          }   

	          $excel_row = 2;
	          $i=1;
	          foreach($data as $row)
	          {
	          	
	           $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->ref_no);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->tender_title);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->part_no);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->part_name);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->part_price);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->drawing_no);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, date('d-m-Y',strtotime($row->delivery_duration)));
	           $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, date('d-m-Y',strtotime($row->delivery_date)));
	           $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->quantity);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row,$row->base_price);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row,$row->norm);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, date('d-m-Y',strtotime($row->start_date)));
	           $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row,date('d-m-Y',strtotime($row->end_date)));
	           $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->price);
	           $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row->rm_price);
	           $i++;
	           $excel_row++;
	          }
	          $object->getActiveSheet()
	            ->getStyle('A1:P1')
	            ->applyFromArray(
	                array(
	                    'fill' => array(
	                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                        'color' => array('rgb' => 'D9D9D9')
	                    )
	                )
	            );

	          $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
	          header('Content-Type: application/vnd.ms-excel');
	          header('Content-Disposition: attachment;filename="Tender.xls"');
	          $object_writer->save('php://output');
	        }
    	}
    	else{
    		redirect('admin/tender/tender_list');
    	}
	}
	public function delete(){
		$id=$this->input->get('id');
		$datas['status']='Trash';
		$datas['modified_on']=date('Y-m-d H:i:s');
		$output=$this->tender_model->update($id,$datas);
		if(!empty($output)){
			redirect('admin/tender/tender_list');
		}    
	}
}
