<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Invite extends Admin_Controller {

	public function tender_invite()
	{

		$data = array();
		if (!empty($_POST['invite'])) {
			$data['search'] = $_POST['invite'];
		}
		$this->load->model('Invite_Model');
        $data['getTenderInvList'] =  $this->Invite_Model->tenderInviteList();
		$data['subview'] = $this->load->view('admin/tender_invite', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function viewtender()
	{
		$data = array();
		$tenderId=$this->uri->segment(4);
		$this->load->model('Invite_Model');
        $data['viewTender'] =  $this->Invite_Model->viewTender($tenderId);
		$data['subview'] = $this->load->view('admin/viewtender', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function send_invite()
	{
		$data = array();
		if (!empty($_POST['sendinvite'])) {
			$data['search'] = $_POST['sendinvite'];
		}
		$this->load->model('Supplier_Register');
        $data["catgoryData"] =  $this->Supplier_Register->Suppliercategory();
        $this->load->model('Invite_Model');
        $tenderId=$this->uri->segment(4);
        $data['getVendorInvList'] =  $this->Invite_Model->VendorInviteList($tenderId);
		$data['subview'] = $this->load->view('admin/send_invite', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
    public function send_invite_mail()
	{
		$ArrayVendor = $_POST["inviteArr"];
		$tenderId = $_POST["tenderid"];
		if (!empty($tenderId)) {
			$this->load->model('Invite_Model');
			$data =  $this->Invite_Model->VendorInvitemail($tenderId,$ArrayVendor);
			if ($data=="success") {
				return "success";
			} if ($data =="failure") {
				return "failure";
			}
		}
	}
	public function invited_tender()
	{
		$data = array();
		$data = array();
		if (!empty($_POST['invite'])) {
			$data['search'] = $_POST['invite'];
		}
		$this->load->model('Invited_Model');
        $data['gettenderInvList'] =  $this->Invited_Model->InvitedList();
		$data['subview'] = $this->load->view('admin/invited_tender', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function get_invited_vendor() {

		$tenderid = $_POST["tenderid"];
		$rfqno = $_POST["rfqno"];
		if (!empty($tenderid)) {
			$queryStm = $this->db->select('*')
                    ->from('tbl_tender_invitations')
                    ->where("tender_id ='$tenderid'")
                    ->order_by('id', 'desc')
                    ->get();
        	$rowcount = $queryStm->num_rows();
	        if ($rowcount > 0) {
	            foreach ($queryStm->result() as $rows) {
	                $vendor_id[] = $rows->vendor_id;
	            }
	            $ArrVendorid = implode(',', $vendor_id);
	            $query = $this->db->select('*')
	                    ->from('tbl_vendors')
	                    ->order_by('vendor_id', 'desc')
	                    ->get();
	            $rowcounts = $query->num_rows();
	            if ($rowcounts > 0) {
	                $data="";
	                $sno=1;
	                foreach ($query->result() as $row) {
	                    $vendorcategory= $row->vendor_category;
	                    $vendorId= $row->vendor_id;

	                    if (!empty($vcategory)) {
	                        $category_id=$vcategory;
	                    } else{
	                        $categorySplit = explode("##",$vendorcategory); 
	                        $category_id= next($categorySplit);
	                    }
	                    

	                    $stmt = $this->db->query("select category_name from tbl_category where  category_id='" . $category_id. "'");
	                    $cname = $stmt->row();
	                    if (!empty($vcategory)) {
	                        $vendor_cate=$cname->category_name;
	                    } else {
	                        $vendor_cate=$cname->category_name."...";
	                    }

	                    $data.= "<tr>
	                        <td>".$sno."</td>
	                        <td>".$row->vendor_code."</td>
	                        <td>".$row->company_name."</td>
	                        <td>".$vendor_cate."</td>
	                        <td>Invited</td>
	                    </tr>";
	                   $sno++;
	                }
	                echo  $data;
	            } else {
	                $data= "<tr>
	                         <td colspan='5'> No records Found</td>
	                    </tr>";
	                echo $data;
	            }
	        }
			// $this->load->model('invited_model');
			// $data =  $this->invited_model->getinvitedvendor($tenderid,$rfqno);
			//return  $data;
		}
	}
}
