<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Vendor extends Admin_Controller {

	

	function __construct() {
        parent::__construct();
        $this->load->model('Vendor_Model');
        $this->load->library('email');

        $config = array (
        	'mailtype'=>'html'
    	);
    }

	public function add_vendor()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/add_vendor', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function edit_vendor()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit_vendor', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function vendor_list()
	{
		$data = array();
		if (!empty($_POST['vendor'])) {
			$data['search'] = $_POST['vendor'];
		}
		$data['vendor_list'] = $this->Vendor_Model->get_vendor_list();
		$data['subview'] = $this->load->view('admin/vendor_list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function get_vendor() {
		$action = $_POST['status'];
		if ($action == 'view') {
			$single_vendor = array();
			$vendors = $this->Vendor_Model->get_single_vendor($_POST['vendor_id']);
			foreach ($vendors as $vendor) {
				$single_vendor['id'] = $vendor->vendor_id;
				$single_vendor['name'] = $vendor->first_name;
				$single_vendor['status'] = $vendor->status;
				$single_vendor['vendor_doc'] = $vendor->supplier_doc;
			}
			echo json_encode($single_vendor);
		} else {
			$vendor_status = $this->Vendor_Model->update_single_vendor($_POST['vendor_id'], $action);
			$this->session->set_flashdata('message', 'Vendor status updated successfully');
			echo json_encode($vendor_status);
		}
	}

	public function update_status() {
		if (isset($_POST['vendor_data'])) {
			$decrpted = $this->tenderapi_decrypt_data($_POST['vendor_data']);
			parse_str($decrpted, $parse_data);
			$vendor = $this->Vendor_Model->update_vendor_status($parse_data);
			if ($vendor) {
				$this->session->set_flashdata('message', 'Vendor status updated successfully');
				echo 'success';
			}
			else
				echo 'failure';
		}
	}

	public function invite_vendor() {
		if (isset($_POST['vendor_data'])) {
			$decrpted = $this->tenderapi_decrypt_data($_POST['vendor_data']);
			parse_str($decrpted, $parse_data);
			$invited = $this->Vendor_Model->add_vendor_invite($parse_data);
			if ($invited) {
				$mail_sent = $this->vendor_invite_email($parse_data);
			}
			else
				echo 'Please try again.';
		}
	}

	public function vendor_invite_email($vendor_details) {
		$this->email->from('ilayabharathi.shenll@gmail.com', 'Ilayabharathi S');
		$this->email->to($vendor_details['email']);

		$this->email->subject('Invitation for TENDER');
		$this->email->set_mailtype("html");
		$this->email->message(
			'<div>
				<p>Invited for TENDER</p>
			</div>'
		);

		if ($this->email->send()) {
			echo 'success';
			$this->session->set_flashdata('message', 'Vendor Invited successfully');
		} else {
			echo $this->email->print_debugger();
		}
	}

	public function tenderapi_decrypt_data($data)
    {
        return base64_decode($data);
    }
}
