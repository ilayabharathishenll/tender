<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Settings extends Admin_Controller {

	public function admin_list()
	{
		$data = array();
		$this->load->model('Settings_Model');
        $data["adminList"] =  $this->Settings_Model->AdminList();
		$data['subview'] = $this->load->view('admin/admin_list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function add_admin()
	{
		$data = array();
		$adminid=$this->uri->segment(4);
		if ($adminid!="") {
			$name = $this->input->post("name"); 
			if(!empty($adminid) && empty($name)) {
				$this->load->model('Settings_Model');
                $data["getadmin"] =  $this->Settings_Model->AdminGet($adminid);
                $data['subview'] = $this->load->view('admin/add_admin', $data, TRUE);
		        $this->load->view('admin/_layout_main', $data);
			} else {
			   $admin_id = $this->input->post("hndid"); 
               $this->load->model('Settings_Model');
               $updatedata =  $this->Settings_Model->Adminupdate($admin_id);
               if(!empty($updatedata)) {
               	  $this->session->set_flashdata('message_success', 'Admin information updated successfully.');
                  redirect("admin/settings/admin_list");
               } 
			}
            
		} else {
			$this->load->model('Settings_Model');
            $admindetails =  $this->Settings_Model->AdminAdded();
            $data['subview'] = $this->load->view('admin/add_admin', $data, TRUE);
		    $this->load->view('admin/_layout_main', $data);
		    if(!empty($admindetails)) {
		    	$this->session->set_flashdata('message_success', 'Admin information added successfully.');
				redirect("admin/settings/admin_list");
		    }
        }
	}
	public function admin_email_exists(){
		$adminid=$_POST["adminid"];
		$email=$_POST["email"];
		if(empty($adminid)){
			$this->db->where('email', $email);
			$query = $this->db->get('tbl_admins');
			if ( $query->num_rows() > 0 ) { 
				echo 'exists';
				
			} else { 
			    echo 'not-exists';
			}
		} else {
			echo 'not-exists';
		}
	}
}
