<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Admin_Tender extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }
    function addtender()
    {
        $part_no    = $this->input->post("part_no");
        $part_name  = $this->input->post("part_name");
        $part_price = $this->input->post("part_price");
        $qunatity   = $this->input->post("qunatity");
        $period     = $this->input->post("period");
        $drawing_no = $this->input->post("drawing_no");
        $datetimedb = date("Y-m-d H:i:s");
        if (!empty($part_no)) {
            $data=array(
                "part_no"=>$part_no,
                "part_name"=>$part_name,
                "part_price"=>$part_price,
                "qunatity"=>$qunatity,
                "tender_change"=>$period,
                "drawing_no"=>$drawing_no,
                "created_date"=>$datetimedb,
                "modified_date"=>$datetimedb,
            );
            $this->db->insert('tbl_tender', $data);
            return $this->db->insert_id();
        }
        // if (!empty($part_no)) {
        //     $insertQry = $this->db->prepare("INSERT INTO tbl_tender(part_no, part_name, part_price, qunatity, tender_change, drawing_no, created_date, modified_date) VALUES(:part_no, :part_name, :part_price, :qunatity, :tender_change, :drawing_no, :created_date, :modified_date)");

        //     $insertQry->execute(array(':part_no' => $part_no, ':part_name' => $part_name, ':part_price' => $part_price, ":qunatity"=>$qunatity, ':tender_change' => $period, ':drawing_no' => $drawing_no,":created_date"=>$datetimedb,":modified_date"=>$datetimedb));

        //     $affected_rows = $insertQry->rowCount();
        //     $insert_id = $this->db->insert_id();
        //     return  $insert_id;
        // } else {
        //     return false;
        // }
    }
}
