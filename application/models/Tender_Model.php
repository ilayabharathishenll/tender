<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');



class Tender_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }
    function add($data){
        $this->db->insert('tbl_tender', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function add_details($data){
        $this->db->insert('tbl_tender_documents', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function update($id,$data){
        //print_r($data);exit;
        $this->db->where('tender_id', $id);
        $this->db->update('tbl_tender', $data);
        return true;
    }
    function tender_list($searcharray=null){
        $this->db->select('*');
        $this->db->from('tbl_tender');
        if(!empty($searcharray)){
            foreach($searcharray as $key=>$value){
                $this->db->like($key,$value);
            }
        }
        $this->db->where('status!=','Trash');
        $query=$this->db->get()->result();
        //echo $this->db->last_query();print_r($query);exit;
        return $query;
    }
    function view_list($id){
        $this->db->select('*');
        $this->db->from('tbl_tender');
        $this->db->where('tender_id',$id);
        $this->db->where('status','Active');
        $query['tender_list']=$this->db->get()->result();
        $query['tender_document']=$this->document_list($id);
        return $query;
    }
    function list_tender($id){
        $this->db->select('*');
        $this->db->from('tbl_tender');
        $this->db->where('tender_id',$id);
        $this->db->where('status','Active');
        $query=$this->db->get()->result();
        return $query;
    }
    function check($id){
        $this->db->select('*');
        $this->db->from('tbl_tender');
        $this->db->where('tender_id',$id);
        $query=$this->db->get()->num_rows();
        if(!empty($query)){
            return "1";
        }
        else{
            return "0";
        }
    }
    function document_list($id){
        $this->db->select('*');
        $this->db->from('tbl_tender_documents');
        if(!empty($id)){
            $this->db->where('tender_id',$id);
        }        
        $this->db->order_by('t_doc_type','Asc');
        $query=$this->db->get()->result();
        //echo $this->db->last_query();print_r($query);exit;
        return $query;
    }

}

