<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Supplier_Register extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Collective');
    }
    function Supplierreg(){
        $catgory   = $this->input->post("catgory");
    	$first_name = $this->input->post("first_name");
        $last_name  = $this->input->post("last_name");
        $email      = $this->input->post("email");
        $passwords  = $this->input->post("password");
        $password   =  $this->Collective->encode($passwords);
        $phone      = $this->input->post("phone");
        $alt_phone  = $this->input->post("alt_phone");
        $landline_no = $this->input->post("landline_no");
        $gender     = $this->input->post("gender");
        $dob        = $this->input->post("dob");
        $address    = $this->input->post("address");
        $p_address  = $this->input->post("permanent_address");
        $state      = $this->input->post("state");
        $country    = $this->input->post("country");
        $company_name  = $this->input->post("company_name");
        $company_regno  = $this->input->post("company_regno");
        $vendor_category = $this->input->post("catgory");

        $datetimedb = date("Y-m-d H:i:s");
        if (!empty($first_name)) {
            $category="##".implode("##",$vendor_category)."##";
            $data=array(
                "first_name"=>$first_name,
                "last_name"=>$last_name,
                "email"=>$email,
                "password"=>$password,
                "phone"=>$phone,
                "phone_secondary"=>$alt_phone,
                "landline"=>$landline_no,
                "date_of_birth"=>date("Y-m-d",strtotime($dob)),
                "gender"=>$gender,
				"address_temp"=>$address,
				"address_permanent"=>$p_address,
				"state"=>$state,
				"country"=>$country,
				"company_name"=>$company_name,
				"company_reg_no"=>$company_regno,
				"status"=>"Inactive",
                "vendor_category"=>$category,
				"created_date"=>$datetimedb,
				"modified_date"=>$datetimedb,
            );
            $this->db->insert('tbl_vendors', $data);
            $regId=$this->db->insert_id();
            if(!empty($regId)) {
                $vendorCode="VEN".date("Y")."-".$regId;
                $data_vendor=array("modified_date"=>$datetimedb,"vendor_code"=>$vendorCode);
                $this->db->where('vendor_id',$regId);
                $this->db->update('tbl_vendors', $data_vendor);
            }
            return $regId;
        }
    }
    function Getprofile($vendor_id) {
        $this->db->select('*');
        $this->db->from('tbl_vendors');
        $this->db->where('vendor_id',$vendor_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            $row = $query->row_array();
            $data="";
            foreach ($query->result() as $row) {
                $data = array(
                    'vendor_id' =>$row->vendor_id,
                    'first_name'=> $row->first_name,
                    'last_name'=> $row->last_name,
                    'email' =>$row->email,
                    'password' => $this->Collective->decode($row->password),
                    'phone'=> $row->phone,
                    'phone_secondary'=>$row->phone_secondary,
                    'landline' => $row->landline,
                    'gender'=>  $row->gender,
                    'date_of_birth'  =>date("m/d/Y",strtotime($row->date_of_birth)),
                    'address_temp' => $row->address_temp,
                    'address_permanent'=> $row->address_permanent,
                    'state'  =>$row->state,
                    'country' => $row->country,
                    'company_name'=>  $row->company_name,
                    'company_reg_no'  =>$row->company_reg_no,
                    'company_reg_year' => $row->company_reg_year,
                    'projects_completed'=>  $row->projects_completed,
                    'projects_ongoing'  =>$row->projects_ongoing,
                    'vendor_category'  =>$row->vendor_category,
                    'comments' => $row->comments,
                );
                return $data;
            }
        } 
    }
    function Updateprofile($vendor_id) {
        $first_name = $this->input->post("first_name");
        $last_name  = $this->input->post("last_name");
        $email      = $this->input->post("email");
        $passwords  = $this->input->post("password");
        $password   = $this->Collective->encode($passwords);
        $phone      = $this->input->post("phone");
        $alt_phone  = $this->input->post("alt_phone");
        $landline_no = $this->input->post("landline_no");
        $gender     = $this->input->post("gender");
        $dob        = $this->input->post("dob");
        $address    = $this->input->post("address");
        $p_address  = $this->input->post("perment_address");
        $state      = $this->input->post("state");
        $country    = $this->input->post("country");
        $company_name  = $this->input->post("company_name");
        $company_regno  = $this->input->post("company_regno");
        $company_regyear = $this->input->post("company_regyear");
        $projects_completed = $this->input->post("projects_completed");
        $ongoing = $this->input->post("ongoing");
        $comments = $this->input->post("comments");
        $vendor_category = $this->input->post("catgory");
        $category="##".implode("##",$vendor_category)."##";
        $datetimedb = date("Y-m-d H:i:s");
        $data=array(
                "first_name"=>$first_name,
                "last_name"=>$last_name,
                "email"=>$email,
                "password"=>$password,
                "phone"=>$phone,
                "phone_secondary"=>$alt_phone,
                "landline"=>$landline_no,
                "date_of_birth"=>date("Y-m-d",strtotime($dob)),
                "gender"=>$gender,
                "address_temp"=>$address,
                "address_permanent"=>$p_address,
                "state"=>$state,
                "country"=>$country,
                "company_name"=>$company_name,
                "company_reg_no"=>$company_regno,
                "company_reg_year"=>$company_regyear,
                "projects_completed"=>$projects_completed,
                "projects_ongoing"=>$ongoing,
                "comments"=>$comments,
                "vendor_category"=>$category,
                "modified_date"=>$datetimedb,
            );
        $this->db->where('vendor_id',$vendor_id);
        $this->db->update('tbl_vendors', $data);
        return true;

    }
    function Suppliercategory() {
        $this->db->from("tbl_category");
        $this->db->order_by("category_name", "asc");
        $query = $this->db->get(); 
        $rowcount = $query->num_rows();
        if($rowcount>0){
            $data = $query->result();
            return $data;
         }  else {
            return false;
        }
    }
}
