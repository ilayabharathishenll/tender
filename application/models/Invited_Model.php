<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Invited_Model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }
    function InvitedList() {
        if (!empty($_POST['invite'])) {
            $invite = $_POST['invite'];
            $rfq_no = $invite['rfq_no'];
            $tender_title = $invite['tender_title'];
            $part_name = $invite['part_name'];
        } else {
            $rfq_no= $tender_title = $part_name ="";
        }
        //$currentdate=date("Y-m-d");
        $query = $this->db->select('*')
                ->from('tbl_tender')
                ->where("ref_no LIKE '%$rfq_no%'")
                ->where("tender_title LIKE '%$tender_title%'")
                ->where("part_name LIKE '%$part_name%'")
                //->where("end_date <= '$currentdate'")
                ->where("status = 'Active'")
                ->order_by('created_on', 'desc')
                ->get();
        return $query->result();
    }
}
