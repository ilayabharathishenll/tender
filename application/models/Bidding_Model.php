<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Bidding_Model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }
    function getbiddingList() {

        if (!empty($_POST['bidding'])) {
            $bidding = $_POST['bidding'];
            
            
            $condition = $searchenddate = $searchstartdate="";
            if (!empty($bidding['start_date']) && empty($bidding['end_date'])) {
                $searchstartdate = date("Y-m-d",strtotime($bidding['start_date']));
                $condition.=" AND tender.start_date ='".$searchstartdate."'";
            }
            if (empty($bidding['start_date']) && !empty($bidding['end_date'])) {
                $searchenddate = date("Y-m-d",strtotime($bidding['end_date']));
                $condition.=" AND tender.end_date ='".$searchenddate."'";
            }
            if (!empty($bidding['start_date']) && !empty($bidding['end_date'])) {
                $searchstartdate = date("Y-m-d",strtotime($bidding['start_date']));
                $searchenddate  = date("Y-m-d",strtotime($bidding['end_date']));
               $condition.=" AND tender.start_date >= '".$searchstartdate. "' and tender.end_date <= '".$searchenddate. "'";
            }
            $searchrfq_no = $bidding['rfq_no'];
            $searchtender_title = $bidding['tender_title'];
            $searchpart_name = $bidding['part_name'];
        } else {
            $condition = $searchstartdate= $searchenddate=$searchrfq_no= $searchtender_title = $searchpart_name ="";
        }
        $currentdate=date("Y-m-d");
        $sessionVendorid=$this->session->userdata('vendor_id');
        $query = $this->db->select('applytender.tender_id, applytender.vendor_id, applytender.price as applyprice, tender.tender_title,tender.tender_title,tender.part_name,tender.ref_no,tender.base_price,tender.part_price,tender.start_date,tender.end_date,tender.price_edit')
            ->from('tbl_apply_tender_audit as applytender')
            ->join('tbl_tender as tender', 'applytender.tender_id = tender.tender_id', 'INNER JOIN')
            ->where("tender.ref_no LIKE '%$searchrfq_no%'")
            ->where("tender.tender_title LIKE '%$searchtender_title%'")
            ->where("tender.part_name LIKE '%$searchpart_name%'".$condition)
            ->where('applytender.vendor_id', $sessionVendorid)
            ->get();
        if ($query->num_rows() > 0)
        {
            $data="";

            foreach ($query->result() as $row) {
                $tender_id = $row->tender_id;
                $vendor_id = $row->vendor_id;
                $applyprice = $row->applyprice;
                $tender_title = $row->tender_title;
                $part_name = $row->part_name;
                $ref_no = $row->ref_no;
                $base_price = $row->base_price;
                $part_price = $row->part_price;
                $start_date = $row->start_date;
                $end_date = $row->end_date;
                $price_edit = $row->price_edit;
                $currentdate=date("Y-m-d");
                //echo $start_date."==>".$end_date;echo"<br>";
                if ($price_edit=="2") {
                   $viewbtn="hide";
                } else {
                    /* case
                        startdate tomorrow 
                        enddate today or previous days
                    */
                    if (($currentdate < $start_date) && ($currentdate < $end_date)) {
                        $viewbtn="hide";                        
                    } elseif($currentdate > $end_date) {
                        $viewbtn="hide";
                    } else { 
                        $stmtaward = $this->db->query("select * from tbl_awarded_tenders where tender_id='" .$tender_id. "'");
                        $countAward= $stmtaward->num_rows();
                        if ($countAward>0) {
                            $awardData = $stmtaward->row();
                            $viewbtn="hide";
                        } else {
                            $viewbtn="show";
                        }
                    }
                }
                
                $stmtcurrent = $this->db->query("select MIN(price) as currentPrice from tbl_apply_tender_audit where tender_id='" .$tender_id. "'");
                $currentCount= $stmtcurrent->num_rows();
                $currentPriceData = $stmtcurrent->row();
                $currentPrice=$currentPriceData->currentPrice;
                
                $data[]= array(
                    'tender_id' =>$row->tender_id,
                    'vendor_id' =>$row->vendor_id,
                    'applyprice'=> $row->applyprice,
                    'tender_title'=> $row->tender_title,
                    'part_name' => $row->part_name,
                    'ref_no'  =>$row->ref_no,
                    'base_price' => $row->base_price,
                    'part_price' => $row->part_price,
                    'start_date' => $row->start_date,
                    'end_date'  => $row->end_date,
                    'currentPrice'=>$currentPrice,
                    'viewbtn' =>$viewbtn,
                );
                
            }

            return $data;
        }
        // print_r($query->result());
    }
    
    function changebidding($tenderId){
        $sessionVendorid=$this->session->userdata('vendor_id');
        $query = $this->db->select('*')
                ->from('tbl_tender')
                ->where("tender_id= '$tenderId'")
                ->get();
        if ($query->num_rows() > 0)
        {
            $data="";
            foreach ($query->result() as $row) {
                $stmtcurrent = $this->db->query("select * from tbl_apply_tender_audit where vendor_id='" .$sessionVendorid. "'");
                $applyCount= $stmtcurrent->num_rows();
                $applyPriceData = $stmtcurrent->row();
                $vendor_id=$applyPriceData->vendor_id;
                $applyprice=$applyPriceData->price;
                $data= array(
                    'tender_id' =>$row->tender_id,
                    'part_no' =>$row->part_no,
                    'part_name'=> $row->part_name,
                    'tender_title'=> $row->tender_title,
                    'part_name' => $row->part_name,
                    'part_price'  =>$row->part_price,
                    'ref_no' => $row->ref_no,
                    'delivery_duration' => $row->delivery_duration,
                    'start_date' => $row->start_date,
                    'end_date'  => $row->end_date,
                    'base_price'=>$row->base_price,
                    'quantity' =>$row->quantity,
                    'description' =>$row->tender_detail,
                    'applyprice' =>$applyprice,
                    'vendor_id'=>$vendor_id,
                );
            }
        }
       return $data;
    }
    function upadtebidding($vendor_id,$tenderID) {
        $your_price=$this->input->post("your_price");
        if (!empty($your_price)) {
            $stmtcurrent = $this->db->query("select MIN(price) as currentPrice from tbl_apply_tender_audit where tender_id='" .$tenderID. "'");
            $currentCount= $stmtcurrent->num_rows();
            $currentPriceData = $stmtcurrent->row();
            $currentPrice=$currentPriceData->currentPrice;
            $datetimedb=date("Y-m-d H:i:s");
            if ($currentPrice > $your_price) {
                $data_vendor=array("created_on"=>$datetimedb,"price"=>$your_price);
                $this->db->where('vendor_id',$vendor_id);
                $this->db->where('tender_id',$tenderID);
                $this->db->update('tbl_apply_tender_audit', $data_vendor);
                return "success";
            } else {
                $data_vendor=array("created_on"=>$datetimedb,"price"=>$your_price);
                $this->db->where('vendor_id',$vendor_id);
                $this->db->where('tender_id',$tenderID);
                $this->db->update('tbl_apply_tender_audit', $data_vendor);
                return "success";
            }
        }
    }
}
