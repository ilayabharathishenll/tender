<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');



class Supplier_Model extends MY_Model {

    function __construct() {

        parent::__construct();

        $this->load->library('session');

        $this->load->model('Collective');

        $config = array (

            'mailtype'=>'html'

        );

        $this->load->library('email');

       

    }



    function SupplierLogin() {

    	$email = $this->input->post("email");

        $password = $this->input->post("password");

        $passwords=$this->Collective->encode($password);



        if(!empty($email) && !empty($passwords)) {

            $stmt = $this->db->query("select * from tbl_vendors where email='" . $email . "' and password='" .$passwords."'");

            $rowcount = $stmt->num_rows();

            if ($rowcount == 1) {

                $data="";

                foreach ($stmt->result() as $row) {

                    $data = array(

                        'vendor_id'  =>$row->vendor_id,

                        'vendor_name' => $row->first_name,

                        'vendor_email'=>  $row->email,

                        'vendor_type'=> "Supplier",

                    );

                    $this->session->set_userdata($data);

                    $vendorid=$row->vendor_id;

                    if(!empty($vendorid)) {

                        $ip=$this->input->ip_address();

                        $data_log = array(

                            'vendor_id'  =>$vendorid,

                            'logged_in' => date("Y-m-d H:i:s"),

                            'ip_address'=>  $ip,

                        );

                        $this->db->insert('tbl_vendor_login_audit',$data_log);

                        $vendor_logid=$this->db->insert_id();

                        $data_log = array("vendorlogid"=>$vendor_logid);

                    }

                     $this->session->set_userdata($data_log);

                    

                    return true;

                }

            } else {

                return false;

            }

        } else {

                return false;

        }

    }

    function logout($vendorlogid) {

      $data=array("logged_out"=>date("Y-m-d H:i:s"));

      $this->db->where('veendor_login_id',$vendorlogid);

      $this->db->update('tbl_vendor_login_audit', $data);

      return true;

    }

    function ForgotPassword() {

        $email = $this->input->post("email");

        if (!empty($email)) {

            $stmt = $this->db->query("select * from tbl_vendors where email='". $email ."'");

            $rowcount = $stmt->num_rows();

            if ($rowcount > 0) {

                $data="";

                foreach ($stmt->result() as $row) {

                    $vendor_id = $row->vendor_id;

                    $vendorid  =  $this->Collective->encode($vendor_id);

                    $first_name = $row->first_name;

                    $last_name = $row->last_name;

                    $email = $row->email;

                    $vendorid=$this->Collective->encode($vendor_id);

                    $HttpPath=base_url();

                    $EmailMessage = '

                        <html>

                            <style>

                                @media screen and (min-width: 320px) {

                                    .container1 {

                                        width: 100%!important;

                                    }

                                }   

                            </style>

                        <table class="container1" width="100%" cellpadding="0" cellspacing="0"  width="600px" style="font-family: verdana;font-size:13px;max-width: 600px;">

                            <tr>

                            <td>Hello '.$first_name.',</td>

                            </tr>

                            <tr style="height:10px"><td></td></tr>

                            <tr>

                            <td>As requested, your Tender Management Reset details are below:</td>

                            </tr>

                            <tr style="height:10px"><td></td></tr>

                            <tr>

                                <td>Full Name : '.$first_name." ".$last_name.'</td>

                            </tr>

                            <tr style="height:2px"><td></td></tr>

                            <tr>

                                <td>User Email : '.$email.'</td>

                            </tr>

                            <tr style="height:2px"><td></td></tr>

                            <tr>

                                <td><a href = "'.$HttpPath.'supplier/login/reset_password/'.$vendorid.'">Click Here To Reset Password</a></td>

                            </tr>

                            <tr style="height:10px"><td></td></tr>

                            <tr>

                            <td>Thank you,<br>Tender Management</td>

                            </tr> 

                        </table>

                        </body>

                        </html>

                        ';

                    $this->email->set_mailtype("html");

                    $this->email->from('karuna.shenll@gmail.com', 'Tender');

                    $this->email->to($email);

                    $this->email->subject('Your Tender Management Login Details');

                    $this->email->message($EmailMessage);

                    if ($this->email->send()) {

                        return "Success";

                    } else {

                        //echo $this->email->print_debugger();

                        return "Notsend";

                    }

                }

                

            } else{

                return "Invaild";

            }

        }

    }

    function ResetPassword ($vendorId) {

        $password = $this->input->post("password");

        if (!empty($vendorId) && !empty($password)) {

            $vendor_id=$this->Collective->decode($vendorId);

            $newpassword=$this->Collective->encode($password);

            $data=array(

                "password"=>$newpassword,

                "modified_date"=>date("Y-m-d H:i:s"),

            );

            $this->db->where('vendor_id',$vendor_id);

            $this->db->update('tbl_vendors', $data);

            return true;

        } else {

            return false;

        }

    }
    function tender_list() {
        $this->db->select("*");
        $this->db->from("tbl_tender_invitations as invitation");
        $this->db->join('tbl_tender as tender','invitation.tender_id=tender.tender_id','inner');
        $this->db->where('tender.status !=', 'Trash');
        $query = $this->db->get();
        echo $this->db->last_query();
        $num_rows = $query->num_rows();
        if($num_rows >0) {
            return $query->result();
        }else {
            return false;
        }
    }

    function view_list($id){
        $this->db->select('*');
        $this->db->from('tbl_tender');
        $this->db->where('tender_id',$id);
        $query['tender_list']=$this->db->get()->result();
        $query['tender_document']=$this->document_list($id);
        return $query;
    }
    function document_list($id){
        $this->db->select('*');
        $this->db->from('tbl_tender_documents');
        if(!empty($id)){
            $this->db->where('tender_id',$id);
        }
        $this->db->order_by('t_doc_type','Asc');
        $query=$this->db->get()->result();
        return $query;
    }
    function add($datas){
        $this->db->insert('tbl_tender_applications', $datas);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function applied_tenders_list() {
        $this->db->select("*");
        $this->db->from("tbl_tender_applications as app");
        $this->db->join('tbl_tender as tender','app.tender_id=tender.tender_id','inner');
        $this->db->where('tender.status !=', 'Trash');
        $query = $this->db->get();
        echo $this->db->last_query();
        $num_rows = $query->num_rows();
        if($num_rows >0) {
            return $query->result();
        }else {
            return false;
        }
    }

}

