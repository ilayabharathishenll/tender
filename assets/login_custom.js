$( function() {
	$("#dob").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$("#frm_register").validate({
	rules: {
		first_name:"required",
		last_name:"required",
		password:{
			required: true
		},
		rpassword:{required: true,equalTo: "#register_password"},
		email:{required:true,email:true,remote: 
			{
				url: base_url+"supplier/login/register_email_exists",
				type: "post",
		    } 
		},
		gender:"required",
		dob:"required",
		address:"required",
		permanent_address:"required",
		state:"required",
		post_code:{required:true,number:true}, 
		phone:{required:true,number:true},
		country:"required",
		company_name:"required",
		company_regno:"required",
		"catgory[]":"required",
	},
	messages: {
		first_name:"Please enter first name",
		last_name:"Please enter last name",
	    position:"Please enter position",
		password:{required:"Please enter password"},
		rpassword:
		{
			required:"Please enter confirm password",
			equalTo: "Your password and confirmation password do not match"
		},
		email:{required:"Please enter email",email:"Please enter vaild email",remote:'Email already used.'},
		gender:"Please select gender",
		dob:"Please select date of birth",
		address:"Please enter temporary address",
		permanent_address:"Please enter permanent address",
		state:"Please enter state",
		phone:{required:"Please enter phone number",number:"Please enter valid phone number"},
		country:"Please select country",
		company_name:"Please enter company name",
		company_regno:"Please enter company regno",
		"catgory[]":"Please select atleast one category",
		
	},
	errorPlacement: function(error, element) {
        if(element.parent('.input-icon').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$("#frm_reset").validate({
	rules: {
		password:{
			required: true
		},
		conpassword:{required: true,equalTo: "#password"},
	},
	messages: {
		password:{required:"Please enter password"},
		conpassword:
		{
			required:"Please enter confirm password",
			equalTo: "Your password and confirmation password do not match"
		},
	},
	errorPlacement: function(error, element) {
        if(element.parent('.input-icon').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});
