function validate_value(input_value)
{
	if(input_value!='' && typeof input_value != 'undefined' && input_value!=0 && input_value != 'failure' )
		return true;
	else
		return false;
}
function tender_validate_data(data)
{
	return btoa(data);
}

function tender_decrypt_data(data)
{
	return atob(data);
}

function tender_serialize(form)
{
	var form_data = jQuery(form).serialize();
	if(validate_value(form_data)) {
		return tender_validate_data(form_data);
	}
}
$(document).ready(function() {
	$('#supplier-quote').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#applied-tenders').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#award-tenders').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#admin-list').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#applied-tender').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#award-list').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#invitetbl').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#send-invite').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#tender-invite').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#tendertbl').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#vendor-list').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#biddingtbl').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});


	var i = 1;
	$('#addmore').on('click', function(){
		var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5"><input type="file" name="upload_terms[]'+i+'" id="upload_terms_'+i+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
		$('#terms').append(newfield);
		i++;
	});
	$(document).on('click','#remove', function(){
		$(this).parent().parent().parent('div').remove();
	});
	i++;

	chelp=0;
	 $('#helpmore').on('click', function(){
	  var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5"></label><div class="col-md-5"><input type="file" name="upload_helpdoc['+chelp+']" id="upload_helpdoc_'+chelp+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removehelp"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
	  $('#helpdoc').append(newfield);
	  chelp++; 
	 });
	 $(document).on('click','#removehelp', function(){
	  $(this).parent().parent().parent('div').remove();
	 });
	 chelp++;

	 cdetail=0;
	 $('#detailmore').on('click', function(){
	  var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5"></label><div class="col-md-5"><input type="file" name="upload_tenderdetail['+cdetail+']" id="upload_tenderdetail_'+cdetail+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removedetail"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
	  $('#detaildoc').append(newfield);
	  cdetail++; 
	 });
	 $(document).on('click','#removedetail', function(){
	  $(this).parent().parent().parent('div').remove();
	 });
	 cdetail++;

	 ctender=0;
	 $('#tenderdocmore').on('click', function(){
	  var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5"><input type="file" name="upload_tenderdoc['+ctender+']" id="upload_tenderdoc_'+ctender+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removetender"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
	  $('#tenderdoc').append(newfield);
	  ctender++;
	 });
	 $(document).on('click','#removetender', function(){
	  $(this).parent().parent().parent('div').remove();
	 });
	 ctender++;

	 ctenderone=0;
	 $('#tenderdoconemore').on('click', function(){
	  var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5"></label><div class="col-md-5"><input type="file" name="tender_docone['+ctenderone+']" id="tender_docone_'+ctenderone+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removetenderone"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
	  $('#tenderdocone').append(newfield);
	  ctenderone++;
	 });
	 $(document).on('click','#removetenderone', function(){
	  $(this).parent().parent().parent('div').remove();
	 });
	 ctenderone++;

	 ctenderonetwo=0;
	 $('#tenderdoctwomore').on('click', function(){
	  var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5"></label><div class="col-md-5"><input type="file" name="tender_doctwo['+ctenderonetwo+']" id="tender_doctwo_'+ctenderonetwo+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removetendertwo"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
	  $('#tenderdoctwo').append(newfield);
	  ctenderonetwo++;
	 });
	 $(document).on('click','#removetendertwo', function(){
	  $(this).parent().parent().parent('div').remove();
	 });
	 ctenderonetwo++;

	$(document).on('click','#vendor-list .action a', function(){
		var id = $(this).attr('data-id');
		var action = $(this).attr('data-value');
		var msg;
		var confirmcheck;
		if (action=="Trash") {
			msg='Are you sure you want to delete this vendor?';
			confirmcheck = confirm(msg);
        } else {
        	confirmcheck = true;
        }
        if (confirmcheck == true) {
			$.ajax({
				url: base_url+'admin/vendor/get_vendor',
				type: 'POST',
				data: {
					vendor_id: id,
					status: action
				},
				dataType: 'json',
				success: function(data) {
					if (data == 1) {
						location.reload();
					} else {
						$('#docPopup h4 span').html(data.name);
						$('#docPopup .vendor-doc a').attr('href', data.vendor_doc);
						$('#docPopup .vendor-detail input').val(data.id);
						$('#docPopup .vendor-detail select').val(data.status);
					}
				}
			});
		} else {
			return false;
		}
	});

	$("#vendor_status").validate({
		submitHandler: function(form) {
			var vendor_data = tender_serialize(form);
			$.ajax({
				url: base_url+'admin/vendor/update_status',
				type: 'POST',
				data: {
					'vendor_data': vendor_data
				},
				success: function(response) {
					if (response == 'success') {
						location.reload();
					} else {
						$('#docPopup').modal('toggle');
					}
				}
			});
		}
	});

	$("#add_vendor").validate({
		rules:{
			'vendor_name':{
				required:true
			},
			'email':{
				required:true,
				email:true
			},
			'contact_no':{
				required:true
			}
		},
		messages:{
			'vendor_name':{
				required:"Please enter vendor name"
			},
			'email':{
				required: "Please enter email",
				email: "Please enter valid email address"
			},
			'contact_no':{
				required:"Please enter contact number",
			}
		},
		submitHandler: function(form) {
			var vendor_data = tender_serialize(form);
			$.ajax({
				url: base_url+'admin/vendor/invite_vendor',
				type: 'POST',
				data: {
					'vendor_data': vendor_data
				},
				success: function(response) {
					location.reload();
				}
			});
		}
	});
});
$("#search_result_length").hide();

$( function() {
	$("#award_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	$("#award_enddate").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	$("#delivery_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	$("#start_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy H:i:S',autoclose: true });
});
$( function() {
	$("#end_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy H:i:S',autoclose: true });
});
$( function() {
	$("#dob").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});

$(document).on("click",".sendinvitepopup",function() {
	$("#inviteappendtable").empty();
	$("#rfqapp").empty();
	var tenderid= $(this).attr("data-id");
	var rfqno= $(this).attr("data-rfq");
	$.ajax({
        url: base_url+"admin/invite/get_invited_vendor",
        type: 'POST',
        data: {tenderid: tenderid,rfqno:rfqno},
        dataType: 'text',
        success: function(data) {
         //console.log(data);
         $("#rfqapp").empty().append(rfqno);
         if(data==""){
			$("#inviteappendtable").empty().append('<tr><td colspan="5" style="text-align:center;"> No records Found</td></tr>');
         } else{
         	$("#inviteappendtable").empty().append(data);
         }
         
         $("#sendPopup").modal("show");
        }
    });
});

$(document).on("click",".documentDownload",function() {
	$("#docPopup").modal("show");
});


$("#frm_profile").validate({
	rules: {
		first_name:"required",
		last_name:"required",
		password:{
			required: true
		},
		conpassword:{required: true,equalTo: "#password"},
		email:{required:true,email:true},
		gender:"required",
		dob:"required",
		address:"required",
		permanent_address:"required",
		state:"required",
		post_code:{required:true,number:true}, 
		phone:{required:true,number:true},
		country:"required",
		company_name:"required",
		company_regno:"required",
		"catgory[]":"required",
	},
	messages: {
		first_name:"Please enter first name",
		last_name:"Please enter last name",
		password:{required:"Please enter password"},
		conpassword:
		{
			required:"Please enter confirm password",
			equalTo: "Your password and confirmation password do not match"
		},
		email:{required:"Please enter email",email:"Please enter vaild email"},
		gender:"Please select gender",
		dob:"Please select date of birth",
		address:"Please enter temporary address",
		permanent_address:"Please enter permanent address",
		state:"Please enter state",
		phone:{required:"Please enter phone number",number:"Please enter valid phone number"},
		country:"Please select country",
		company_name:"Please enter company name",
		company_regno:"Please enter company regno",
		"catgory[]":"Please select atleast one catgory",
		
	},
});

$("#frm_addadmin").validate({
	rules: {
		name:"required",
		email:{required:true,email:true,unique:true},
		password:{
			required: true
		},
		conpassword:{required: true,equalTo: "#password"},
		phone:{required:true,number:true},
	},
	messages: {
		name:"Please enter name",
		email:{required:"Please enter email",email:"Please enter vaild email",unique:"Email already exists"},
		password:{required:"Please enter password"},
		conpassword:
		{
			required:"Please enter confirm password",
			equalTo: "Your password and confirmation password do not match"
		},
		
		phone:{required:"Please enter phone number",number:"Please enter valid phone number"},
	},
});

$.validator.addMethod('unique',function(value)
	{
		var email=$("#email").val();
		var adminid=$('input[name="hndid"]').val();
		var result=$.ajax({
					type:"POST",
					async:false,
					url:base_url+"admin/settings/admin_email_exists",
					data:{"email":email,"adminid":adminid},
					dataType:"text"});
	 if(result.responseText=="not-exists"){
		 return true;
	 }
	 else{ 
		return false;
	 }
},"Email already already exists!");

function checkAllExp(Element)
{
	chk_len=$('input[id=checkbox]').length;
	if(Element.checked){
	    if(chk_len>0){
	        $('input[id=checkbox]').prop('checked', true);
	    }
	}
	else{
	    if(chk_len>0){
	        $('input[id=checkbox]').prop('checked', false);
	    }
	}
}
function eachChk()
{
	chk_len=$('input[id=checkbox]').length;
	chk_len1=$('input[id=checkbox]:checked').length;
	if(chk_len==chk_len1){
	    $('input[name=export_all]').prop('checked', true);
	}
	else{
	    $('input[name=export_all]').prop('checked', false);
	}
}
$(document).ready(function(){
    $("#sendinvitemail").click(function(){
        chk_len = $('input[id=checkbox]:checked').length;
        if(chk_len==0)
        {
            alert("Please select atleast one invite vendor");
            return false;
        }
        else
        {
        	var checked = [];
        	$("input[name='checkbox[]']:checked").each(function ()
			{
			    checked.push(parseInt($(this).val()));
			});
			var tenderID=$("#hnd_tender").val();
			//console.log(checked);
			var wurl      = window.location.href; 
            $.ajax({
		        url: base_url+"admin/invite/send_invite_mail",
		        type: 'POST',
		        data: {inviteArr: checked,tenderid:tenderID},
		        dataType: 'text',
		        success: function(data) {
		          if (data !="failure"){
		          	// console.log(data);
		          	// console.log(wurl);

		          	$("#alertappend").empty().append('<div class="alert alert-success  alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Vendor inviate email sent successfully.</div>');
                    $("#alertappend").show();
                    setTimeout(function(){ window.location.href=wurl; },4000);
                    
		          } else {
                    $("#alertappend").empty().append('<div class="alert alert-danger  alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Vendor inviate email not sent.</div>');
                    $("#alertappend").show();
		          }
		           
		        }
		    });
        }
    });
});